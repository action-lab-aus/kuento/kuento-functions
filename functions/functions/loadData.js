const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const FieldValue = require('firebase-admin').firestore.FieldValue;
const bucket = admin.storage().bucket();
const fs = require('fs');
const os = require('os');
const path = require('path');
const xlsx = require('xlsx');
const { loadTestData } = require('../utils/loadTestData');

/**
 * HTTP function for generating and pushing the test data into Firestore
 */
exports.loadTestData = functions.https.onRequest(async (req, res) => {
  await loadTestData();
  res.status(200).send('Finish loading test data...');
});

/**
 * HTTP function for updating langs objects to status `readyformanualtranslate`
 */
exports.updateLangsObject = functions.https.onRequest(async (req, res) => {
  const submissionSnapshot = await db
    .collection('submissions')
    .where('status', '==', 'readyformanualtranslate')
    .get();

  mediaPromises = [];
  submissionSnapshot.docs.forEach(doc => {
    const mediaRefs = doc.data().media;
    for (let i = 0; i < mediaRefs.length; i++) {
      mediaPromises.push(mediaRefs[i].get());
    }
  });
  const mediaResults = await Promise.all(mediaPromises);

  langsPromises = [];
  for (let i = 0; i < mediaResults.length; i++) {
    const mediaResult = mediaResults[i];
    const langsRefs = mediaResult.data().langs;
    if (langsRefs) {
      for (let j = 0; j < langsRefs.length; j++) langsPromises.push(langsRefs[j].get());
    }
  }
  const langsResults = await Promise.all(langsPromises);

  const langsUpdatePromises = [];
  for (let i = 0; i < langsResults.length; i++) {
    const langsResult = langsResults[i];
    const id = langsResult.id;
    const data = langsResult.data();
    if (!data.original) {
      langsUpdatePromises.push(
        db
          .collection('langs')
          .doc(id)
          .update({ status: 'readyformanualtranslate' }),
      );
    }
  }

  await Promise.all(langsUpdatePromises);

  res.status(200).send('Finish updating test data...');
});

/**
 * HTTP function for loading static content from the pre-defined spreadsheet, and
 * create media object accordingly based on the type for the translation pipeline
 */
exports.loadStaticContent = functions.https.onRequest(async (req, res) => {
  try {
    // Load the spreadsheet using xlsx
    const buffer = fs.readFileSync(path.join(__dirname, '../scripts/staticComms.xlsx'));
    const workbook = xlsx.read(buffer, { type: 'buffer' });
    const sheetNameList = workbook.SheetNames;
    const xlData = xlsx.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]]);

    const staticMediaPromises = [];
    // Loop through the rows and parse the media to be translated
    for (let i = 0; i < xlData.length; i++) {
      const { type, content, description, en } = xlData[i];
      const newMediaRef = db.collection('media').doc();
      const newMediaObj = {
        type: type,
        content: content,
        createdAt: FieldValue.serverTimestamp(),
      };

      // Add YouTube link for static videos and create langs object for all 4 major languages
      if (type === 'static_video') {
        newMediaObj['youtube'] = description;
        newMediaObj['status'] = 'readyformanualsubtitle';
        newMediaObj['srcLang'] = 'en';
        newMediaObj['uploadedAt'] = FieldValue.serverTimestamp();
        const dbRef = db.collection('config').doc('stats');
        await dbRef.update({
          readyformanualsubtitle: FieldValue.increment(1),
        });
      } else {
        newMediaObj['modality'] = 'text';
        // Generate `langs` collection for text content
        const langsRef = db.collection('langs').doc();
        newMediaObj['langs'] = [langsRef];
        staticMediaPromises.push(
          langsRef.set({
            srcLang: 'en',
            srcText: en,
            original: true,
            media: `media/${newMediaRef.id}`,
            createdAt: FieldValue.serverTimestamp(),
          }),
        );

        if (type === 'static_image' || type === 'static_locale' || type === 'static_article')
          newMediaObj['description'] = description ? description : null;
      }

      staticMediaPromises.push(newMediaRef.set(newMediaObj));
    }

    await Promise.all(staticMediaPromises);
    res.status(200).send('Successfully loaded static content...');
  } catch (error) {
    console.error(error);
    res.status(400).send('error: ', error);
  }
});

/**
 * Export static content (with type `static_video`, `static_image`, or `static_article`
 * into a spreadsheet and save it to Firebase storage for downloading
 */
exports.exportStaticContent = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '1GB',
  })
  .https.onRequest(async (req, res) => {
    // Retrieve all the media with type `static_*` and create an array of objects
    const videoData = await getStaticVideos();
    const imageAndArticleData = await getStaticImagesAndArticles();
    const data = [...videoData, ...Object.values(imageAndArticleData)];

    // Create and save the worksheet into local directory
    const workbook = xlsx.utils.book_new();
    const worksheet = xlsx.utils.json_to_sheet(data);
    xlsx.utils.book_append_sheet(workbook, worksheet, 'staticComms');

    const exportFileName = 'staticComms.xlsx';
    const tempFilePath = path.join(os.tmpdir(), exportFileName);
    xlsx.writeFile(workbook, tempFilePath);

    // Upload the worksheet to Firebase storgae
    await bucket.upload(tempFilePath, {
      destination: `static/${exportFileName}`,
    });

    fs.unlinkSync(tempFilePath);
    res.status(200).send('Successfully exported translated static content...');
  });

/**
 * Get objects with type `static_video`
 */
async function getStaticVideos() {
  // Fetch all the media with type `static_video`
  const videoSnapshot = await db
    .collection('media')
    .where('type', '==', 'static_video')
    .get();

  // Create entry for each `static_video` object
  const videoData = [];
  videoSnapshot.forEach(doc => {
    const data = doc.data();
    videoData.push({
      type: data.type,
      content: data.content,
      description: data.youtube,
    });
  });

  return videoData;
}

/**
 * Get objects with type `static_image` or `static_article`
 */
async function getStaticImagesAndArticles() {
  try {
    // Fetch all the media with type `static_image` & `static_article`
    const snapshot = await db
      .collection('media')
      .where('type', 'in', ['static_image', 'static_article', 'static_locale'])
      .get();

    const dataObj = [];
    const langsPromises = [];
    const snapshotRef = [];

    // Fetch the langs object for each `static_image` & `static_article`
    snapshot.forEach(doc => {
      const langsRefs = doc.data().langs;
      for (let i = 0; i < langsRefs.length; i++) {
        langsPromises.push(langsRefs[i].get());
        snapshotRef.push({
          type: doc.data().type,
          content: doc.data().content,
          description: doc.data().description,
        });
      }
    });

    const results = await Promise.all(langsPromises);

    for (let i = 0; i < results.length; i++) {
      // Get the result of the get `langs` promise
      const result = results[i].data();
      if (dataObj[result.media]) {
        if (result.original) dataObj[result.media][result.srcLang] = result.srcText;
        // else dataObj[result.media][result.targetLang] = result.status === 'finalised' ? result.targetText : 'N/A';
        else dataObj[result.media][result.targetLang] = result.targetText;
      } else {
        dataObj[result.media] = {
          type: snapshotRef[i].type,
          content: snapshotRef[i].content,
          description: snapshotRef[i].description,
        };

        if (result.original) dataObj[result.media][result.srcLang] = result.srcText;
        // else dataObj[result.media][result.targetLang] = result.status === 'finalised' ? result.targetText : 'N/A';
        else dataObj[result.media][result.targetLang] = result.targetText;
      }
    }

    return dataObj;
  } catch (error) {
    functions.logger.error(error);
  }
}
