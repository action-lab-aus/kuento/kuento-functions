const functions = require('firebase-functions');
const admin = require('firebase-admin');
const environment = functions.config();
const db = admin.firestore();
const storage = admin.storage();
const FieldValue = admin.firestore.FieldValue;
const { getNoOfVideos, uploadCaptionToS3 } = require('../utils/utils');
const path = require('path');
const os = require('os');
const fs = require('fs');
const parseSRT = require('srt-to-json');
const AWS = require('aws-sdk');
AWS.config.loadFromPath(path.join(__dirname, '/awsConfig.json'));
AWS.config.mediaconvert = { endpoint: environment.aws.mediaconvert_endpoint };
const S3 = new AWS.S3();
// const request = require('request');
const BUCKET_NAME = 'cop27';
const { getMediaConvertParamsEditing } = require('../utils/mediaConvertParams.js');
const request = require('request');

async function downloadVideo(fileBucket, filePath, fileName) {
  const bucket = admin.storage().bucket(fileBucket);
  const tempFilePath = path.join(os.tmpdir(), fileName);
  await bucket.file(filePath).download({ destination: tempFilePath });
  return tempFilePath;
}

exports.onRapidEdit = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '4GB',
  })
  .storage.object()
  .onFinalize(async object => {
    const fileBucket = object.bucket;
    const tempFilePath = object.name;
    const fileName = path.basename(tempFilePath);
    const contentType = object.contentType;

    // functions.logger.info('KEY: ', AWS.config.credentials.accessKeyId);

    functions.logger.info('fileBucket: ', fileBucket);
    functions.logger.info('fileName: ', fileName);
    functions.logger.info('contentType: ', contentType);
    const filePath = `dropbox/${fileName}`;
    functions.logger.info('tempFilePath: ', tempFilePath);

    // Return if the file is not uploaded to the dropbox folder
    if (!tempFilePath.startsWith('dropbox')) return;

    try {
      // Check whether all videos for the submission have been uploaded, and change the
      // submission status to be `submitted` if so
      // await checkAndUpdateSubmissionStatus(tempFilePath.split('/')[1]);

      // Start transcoding & transcription pipeline
      const localFileName = await downloadVideo(fileBucket, tempFilePath, fileName);
      functions.logger.info('localFileName: ', localFileName);

      // Read content from the file
      const fileContent = fs.readFileSync(localFileName);

      // Setting up S3 upload parameters
      const params = {
        Bucket: BUCKET_NAME,
        Key: filePath,
        Body: fileContent,
      };

      await S3.upload(params).promise();
      s3Url = `s3://${BUCKET_NAME}/${filePath}`;
      fs.unlinkSync(localFileName);

      // Trigger the transcoding and transcription job once the video being uploaded to S3

      const assetPath = `s3://cop27/assets/pre`;
      const output = `s3://cop27/dropbox/${fileName}/hd_video`;

      // console.log(newData.submitted_by);

      let edit = {
        timeline: [],
      };

      edit.timeline.push({
        src: `${assetPath}/intro_en.mp4`,
        overlay: false,
      });

      edit.timeline.push({
        src: s3Url,
        overlay: false,
      });

      edit.timeline.push({
        src: `${assetPath}/outro_en.mp4`,
        overlay: false,
      });

      edit.watermark = `${assetPath}/watermark_en.mov`;

      // console.log(edit);

      // console.log(edit);

      const params2 = getMediaConvertParamsEditing(edit, '', output);

      // console.log(params);

      // return;

      return new AWS.MediaConvert({ apiVersion: '2017-08-29' }).createJob(params2).promise();
    } catch (error) {
      throw error;
    }
  });
