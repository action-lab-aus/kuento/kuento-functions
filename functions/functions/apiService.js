const functions = require('firebase-functions');
const environment = functions.config();
const function_url = environment.service.function_url;
const admin = require('firebase-admin');
const FieldValue = require('firebase-admin').firestore.FieldValue;
const db = admin.firestore();
const auth = admin.auth();
const express = require('express');
const cors = require('cors');
const axios = require('axios');
const app = express();
app.use(cors({ origin: true }));

// Get the config object from Firestore
app.get('/config', async (req, res) => {
  const snapshot = await db
    .collection('config')
    .doc('meta')
    .get();
  res.status(200).send(snapshot.data());
});

// Check whether the current user is a linkedin user
app.get('/linkedin/:uid', async (req, res) => {
  const user = (await db
    .collection('users')
    .doc(req.params.uid)
    .get()).data();

  const linkedinId = user.linkedin_id;
  res.status(200).send(linkedinId ? true : false);
});

// Update submission with LinkedIn post link
app.post('/submissions/linkedin', async (req, res) => {
  // Check authorization header of the request
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))
    return res.status(403).send('Unauthorized');

  const idToken = req.headers.authorization.split('Bearer ')[1];
  try {
    // Decode the id token for authentication
    const decodedIdToken = await auth.verifyIdToken(idToken);
    functions.logger.log('Firestore id token correctly decoded: ', decodedIdToken);

    const submissionRef = db.collection('submissions').doc(req.body.sid);
    const submissionUid = (await submissionRef.get()).data().submitted_by.id;

    if (submissionUid !== decodedIdToken.uid) return res.status(403).send('Unauthorized');

    await db
      .collection('submissions')
      .doc(req.body.sid)
      .update({ linkedinPost: req.body.linkedinPost });
    return res.status(200).send('Update Successful');
  } catch (error) {
    functions.logger.error('Error while updating submission:', error);
    return res.status(403).send({ status: error.status });
  }
});

// Create LinkedIn post with the given content and video link
app.post('/linkedin', async (req, res) => {
  functions.logger.info('req.body.content: ', req.body.content);
  functions.logger.info('req.body.youtube: ', req.body.youtube);
  functions.logger.info('req.body.uid: ', req.body.uid);

  // Check authorization header of the request
  if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))
    return res.status(403).send('Unauthorized');

  // Get Firebase id token from the request header
  const idToken = req.headers.authorization.split('Bearer ')[1];
  try {
    // Decode the id token for authentication
    const decodedIdToken = await auth.verifyIdToken(idToken);
    functions.logger.log('Firestore id token correctly decoded: ', decodedIdToken);

    const user = (await db
      .collection('users')
      .doc(req.body.uid)
      .get()).data();
    functions.logger.info('user: ', user);

    // Get the linkedin id and access token
    const linkedinId = user.linkedin_id;
    const tokenRef = user.access_token;
    const accessToken = (await db.doc(tokenRef).get()).data().access_token;
    if (!linkedinId || !accessToken) return res.status(400).send('Invalid User Account');

    // Construct the linkedin request data
    const data = {
      author: '',
      lifecycleState: 'PUBLISHED',
      specificContent: {
        'com.linkedin.ugc.ShareContent': {
          shareCommentary: {
            text: '',
          },
          shareMediaCategory: 'NONE',
        },
      },
      visibility: {
        'com.linkedin.ugc.MemberNetworkVisibility': 'CONNECTIONS',
      },
    };
    data.author = `urn:li:person:${linkedinId}`;
    data.specificContent['com.linkedin.ugc.ShareContent'].shareCommentary.text = req.body.content;
    data.specificContent['com.linkedin.ugc.ShareContent'].shareMediaCategory = 'ARTICLE';

    const media = [
      {
        status: 'READY',
        originalUrl: req.body.youtube,
      },
    ];

    data.specificContent['com.linkedin.ugc.ShareContent'].media = media;

    const postRes = await axios.post('https://api.linkedin.com/v2/ugcPosts', data, {
      headers: {
        'X-Restli-Protocol-Version': '2.0.0',
        Authorization: `Bearer ${accessToken}`,
      },
    });

    return res.status(201).send({ status: postRes.status, postUrn: postRes.data.id });
  } catch (error) {
    functions.logger.error('Error while verifying Firebase ID token:', error);
    return res.status(403).send({ status: error.status });
  }
});

// Filter & pagination endpoint for users
app.post('/users', async (req, res) => {
  const { lang, region, limitTo, startFrom } = req.body;
  try {
    // Set up reference for submission
    let ref = db.collection('users');
    ref = ref.where('isParticipant', '==', true);

    // Check the request body and set the where statement accordingly
    if (lang) ref = ref.where('language', '==', lang);
    if (region) ref = ref.where('region', '==', region);

    // Order by createdAt field
    ref = ref.orderBy('user_name', 'asc');

    // Set the startAfter filter
    if (startFrom) {
      const startFromDoc = await db
        .collection('users')
        .doc(startFrom)
        .get();
      ref = ref.startAfter(startFromDoc);
    }

    // Set query limit for documents
    if (limitTo) ref = ref.limit(limitTo);

    const userSnaps = await ref.get();
    // functions.logger.info(userSnaps);

    // Return the last doccument for pagniation
    // const last = userSnaps.docs[userSnaps.docs.length - 1];
    const userDocs = userSnaps.docs.map(doc => doc.data());
    userDocs.map(user => (user.submissions = []));

    // Add user id for the userDocs
    for (let i = 0; i < userSnaps.size; i++) userDocs[i].id = userSnaps.docs[i].id;

    const userRefs = userSnaps.docs.map(doc => doc.ref);
    const userIds = userSnaps.docs.map(doc => doc.id);

    // Fetch submissions for each user
    const subPromises = [];
    for (let i = 0; i < userRefs.length; i++) {
      const userRef = userRefs[i];
      subPromises.push(
        db
          .collection('submissions')
          .where('submitted_by', '==', userRef)
          .get(),
      );
    }

    // Parse all the submission object results
    const subResults = await Promise.all(subPromises);
    for (let i = 0; i < subResults.length; i++) {
      if (subResults[i].size > 0) {
        const subs = subResults[i].docs;
        subs.forEach(sub => {
          const submittedBy = sub.data().submitted_by;
          // functions.logger.info('submittedBy: ', submittedBy.id);
          const idx = userIds.indexOf(submittedBy.id);

          // Remove unused data to be returned
          const submissionData = sub.data();
          delete submissionData.media;
          delete submissionData.submitted_by;
          userDocs[idx].submissions.push(submissionData);
        });
      }
    }

    // const finalDocs = _.filter(userDocs, user => {
    //   functions.logger.info(user.submissions.length);
    //   return user.submissions.length !== 0;
    // });

    res.status(200).send({ userDocs });
  } catch (error) {
    functions.logger.error(error);
    res.status(500).send('Error occurred when fetching users...');
  }
});

// Get randomised highlighted submissions for the landing page
app.get('/submissions/highlights', async (req, res) => {
  try {
    const subSnaps = await db
      .collection('submissions')
      .where('status', '==', 'edited')
      .get();
    const count = subSnaps.size;
    const idxArr = getRandomIndex(count, 4);

    let subDocs = [];
    for (let i = 0; i < idxArr.length; i++) subDocs.push(subSnaps.docs[idxArr[i]]);
    subDocs = subDocs.map(doc => doc.data());

    // Fetch all media objects
    const mediaPromises = [];
    const mediaIdx = [];
    let idx = 0;
    for (let i = 0; i < subDocs.length; i++) {
      const sub = subDocs[i];
      for (let j = 0; j < sub.media.length; j++) {
        const media = sub.media[j];
        mediaPromises.push(media.get());
        mediaIdx[idx] = [i, j];
        idx++;
      }
    }

    // Parse all the media object results
    const mediaResults = await Promise.all(mediaPromises);
    for (let i = 0; i < mediaResults.length; i++) {
      const data = mediaResults[i].data();
      if (data.type === 'hd_video') {
        const loc = mediaIdx[i];
        subDocs[loc[0]].media = data;
        const tmp = subDocs[loc[0]];
        delete tmp.submitted_by;
        subDocs[loc[0]] = tmp;
      }
    }

    res.status(200).send({ subDocs });
  } catch (error) {
    functions.logger.error(error);
    res.status(500).send('Error occurred when fetching highlighted submissions...');
  }
});

// Filter & pagination endpoint for submissions
app.post('/submissions', async (req, res) => {
  const { phase, lang, region, tags, limitTo, startFrom } = req.body;
  try {
    // Set up reference for submission
    let ref = db.collection('submissions');
    ref = ref.where('status', '==', 'edited');
    ref = ref.where('accepted', '==', true);

    // Check the request body and set the where statement accordingly
    if (phase) ref = ref.where('phase', '==', phase);
    if (lang) ref = ref.where('language', '==', lang);
    if (region) ref = ref.where('region', '==', region);
    if (tags) ref = ref.where('tags', 'array-contains-any', tags);

    // Order by createdAt field
    ref = ref.orderBy('createdAt', 'desc');

    // Set the startAfter filter
    if (startFrom) {
      const startFromDoc = await db
        .collection('submissions')
        .doc(startFrom)
        .get();
      ref = ref.startAfter(startFromDoc);
    }

    // Set query limit for documents
    if (limitTo) ref = ref.limit(limitTo);

    const subSnaps = await ref.get();

    // Return the last doccument for pagniation
    // const last = subSnaps.docs[subSnaps.docs.length - 1];
    const subDocs = subSnaps.docs.map(doc => doc.data());
    const subIds = subSnaps.docs.map(doc => doc.id);

    // Fetch all user objects
    const userPromises = [];
    for (let i = 0; i < subDocs.length; i++) {
      const sub = subDocs[i];
      userPromises.push(sub.submitted_by.get());
    }
    const userResults = await Promise.all(userPromises);

    // Parse all the user object results
    for (let i = 0; i < userResults.length; i++) {
      const data = userResults[i].data();
      subDocs[i].submitted_by = data;
    }

    // Fetch all media objects
    const mediaPromises = [];
    const mediaIdx = [];
    let idx = 0;
    for (let i = 0; i < subDocs.length; i++) {
      const sub = subDocs[i];
      subDocs[i].id = subIds[i];
      for (let j = 0; j < sub.media.length; j++) {
        const media = sub.media[j];
        mediaPromises.push(media.get());
        mediaIdx[idx] = [i, j];
        idx++;
      }
    }

    // Parse all the media object results
    const mediaResults = await Promise.all(mediaPromises);
    for (let i = 0; i < mediaResults.length; i++) {
      const data = mediaResults[i].data();
      const loc = mediaIdx[i];
      subDocs[loc[0]].media[loc[1]] = data;
    }

    // // Fetch all lang objects
    // const langPromises = [];
    // const langIdx = [];
    // idx = 0;
    // for (let i = 0; i < subDocs.length; i++) {
    //   const sub = subDocs[i];
    //   for (let j = 0; j < sub.media.length; j++) {
    //     const media = sub.media[j];
    //     if (media.langs) {
    //       for (let k = 0; k < media.langs.length; k++) {
    //         const lang = media.langs[k];
    //         langPromises.push(lang.get());
    //         langIdx[idx] = [i, j, k];
    //         idx++;
    //       }
    //     }
    //   }
    // }

    // // Parse all the lang object results
    // const langResults = await Promise.all(langPromises);
    // for (let i = 0; i < langResults.length; i++) {
    //   const data = langResults[i].data();
    //   const loc = langIdx[i];
    //   subDocs[loc[0]].media[loc[1]].langs[loc[2]] = data;
    // }

    res.status(200).send({ subDocs });
  } catch (error) {
    functions.logger.error(error);
    res.status(500).send('Error occurred when fetching submissions...');
  }
});

// Get basic statistics of the competition including admin users, number of submissions,
// number of participants, etc.
app.get('/statistics', async (req, res) => {
  // Fetch all people in the translation, transcription, and verification team
  const userSnapshots = await db.collection('config/userRoles/users').get();
  const userDocs = userSnapshots.docs;

  const userPromises = [];
  userDocs.forEach(user => {
    const userEmail = user.id;
    userPromises.push(auth.getUserByEmail(userEmail));
  });

  const results = await Promise.all(userPromises.map(p => p.catch(e => e)));
  const userResults = results.filter(result => !(result instanceof Error));
  // console.log('userResults: ', userResults);

  const users = [];
  userResults.forEach(user => {
    users.push({ displayName: user.displayName, email: user.email, photoURL: user.photoURL });
  });

  // Fetch basic statistics to be rendered on the landing page
  const phase_1 = (await db
    .collection('submissions')
    .where('phase', '==', '1')
    .get()).size;

  const phase_2 = (await db
    .collection('submissions')
    .where('phase', '==', '2')
    .get()).size;

  const phase_submit = (await db
    .collection('submissions')
    .where('phase', '==', 'submit')
    .get()).size;

  const submissionStats = { phase_1, phase_2, phase_submit };

  const allUsers = (await db.collection('users').get()).size;
  const allAdmin = (await db.collection('config/userRoles/users').get()).size;

  res.status(200).send({ admin: users, submissionStats: submissionStats, noOfParticipants: allUsers - allAdmin });
});

// Endpoint for creating judging editing jobs (triggered by specific users only)
// app.post('/judging', async (req, res) => {
//   functions.logger.info('req.headers.authorization: ', req.headers.authorization);

//   // Check authorization header of the request
//   if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer '))
//     return res.status(403).send('Unauthorized');

//   // Get Firebase id token from the request header
//   const idToken = req.headers.authorization.split('Bearer ')[1];
//   try {
//     // Decode the id token for authentication
//     const decodedIdToken = await auth.verifyIdToken(idToken);
//     functions.logger.log('Firestore id token correctly decoded: ', decodedIdToken);

//     // Validate the user role, if not a superadmin user, return unauthorized error
//     const roles = (await db
//       .collection('config')
//       .doc('userRoles')
//       .collection('users')
//       .doc(decodedIdToken.email)
//       .get()).data().roles;

//     if (!roles.includes('superadmin')) return res.status(403).send('Unauthorized');

//     // Otherwise, create the judging allocation jobs on Firestore
//     const judgingAllocation = await db.collection('judgingallocation').get();
//     const judgingAllocationPromises = [];

//     // Fetch documents within the judging allocation collection
//     judgingAllocation.docs.forEach(doc => {
//       judgingAllocationPromises.push(
//         db
//           .collection('judgingallocation')
//           .doc(doc.id)
//           .get(),
//       );
//     });
//     const allocations = await Promise.all(judgingAllocationPromises);

//     // Group the allocations by submissions
//     const submissionResults = {};
//     for (let i = 0; i < allocations.length; i++) {
//       const tasks = allocations[i].data().tasks;
//       for (let j = 0; j < tasks.length; j++) {
//         const sid = tasks[j].submission.id;
//         const results = tasks[j].result;
//         if (results) {
//           if (submissionResults[sid]) {
//             let noOfResults = submissionResults[sid].noOfResults;
//             submissionResults[sid].noOfResults = ++noOfResults;
//           } else {
//             submissionResults[sid] = {};
//             submissionResults[sid].noOfResults = 1;
//           }
//         }
//       }
//     }

//     // Get number of judges required for each submissions
//     const noOfJudges = (await db
//       .collection('config')
//       .doc('meta')
//       .get()).data().noOfJudges;

//     // Filter the submissions with all judging done
//     const judgingDone = [];
//     Object.keys(submissionResults).forEach(sid => {
//       if (submissionResults[sid].noOfResults === 3 * noOfJudges) judgingDone.push(sid);
//     });
//     functions.logger.info('judgingDone: ', judgingDone);

//     const judgingEditJobPromises = [];
//     for (let i = 0; i < judgingDone.length; i++) {
//       judgingEditJobPromises.push(
//         db
//           .collection('judgingEditJobs')
//           .doc(judgingDone[i])
//           .set({
//             status: 'pending',
//             createdAt: FieldValue.serverTimestamp(),
//           }),
//       );
//     }

//     await Promise.all(judgingEditJobPromises);

//     // Kickstart the first judging editing jobs
//     const judgingEditJobsSnap = await db
//       .collection('judgingEditJobs')
//       .where('status', '==', 'pending')
//       .limit(1)
//       .get();

//     // Return if no item on the queue
//     const judgingEditJobDoc = judgingEditJobsSnap.docs[0];
//     if (!judgingEditJobDoc) return res.status(201).send('No judging editing job...');

//     const response = await axios.get(`${function_url}feedbackEdit/`, {
//       params: { submission_id: judgingEditJobsSnap.docs[0].id },
//     });
//     functions.logger.info('response: ', response);

//     return res.status(200).send('Successfully created judging editing jobs...');
//   } catch (error) {
//     functions.logger.error('Error while start judging editing jobs:', error);
//     return res.status(403).send({ status: error.status });
//   }
// });

/**
 * Generate random index array based on the total count and array size required
 *
 * @param {*} count Count of the random number
 * @param {*} arrSize Total size of the array required for random number generation
 */
function getRandomIndex(count, arrSize) {
  if (count < arrSize) return Array.from(Array(count).keys());

  const idxArr = [];
  while (idxArr.length < arrSize) {
    const idx = Math.floor(Math.random() * count);
    if (idxArr.indexOf(idx) === -1) idxArr.push(idx);
  }
  return idxArr;
}

exports.api = functions
  .runWith({
    memory: '1GB',
  })
  .https.onRequest(app);
