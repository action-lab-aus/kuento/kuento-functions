const functions = require('firebase-functions');
const admin = require('firebase-admin');
const auth = admin.auth();
// const environment = functions.config();
const db = admin.firestore();
const _ = require('lodash');

exports.getScreen = functions.https.onCall(async (data, context) => {
  let startAfter = data.startAfter ? await db.collection('submissions').doc(data.startAfter).get() : null;

  // console.log(startAfter);

  //number returned
  let count = data.limit || 10;

  let maxRegion = data.max || 2;

  let toSend = [];

  let nextOne = null;
  // let getMore = true;

  let query = db.collection('submissions').where('status', '==', 'edited').orderBy('createdAt', 'desc');
  //if phase set
  if (data.phase) query = query.where('phase', '==', data.phase);
  //if part way through the list:
  if (startAfter) query = query.startAfter(startAfter);

  let allSubmissions = (await query.get()).docs;

  // console.log(allSubmissions);

  while (toSend.length < count) {
    nextOne = allSubmissions.shift();
    if (nextOne) {
      if (canUse(maxRegion, toSend, nextOne)) toSend.push({ ..._.omit(nextOne.data(), ['media']), id: nextOne.id });
    } else {
      break;
    }
  }

  let promises = toSend.map(async doc => {
    // console.log(doc.submitted_by);
    return {
      ...doc,
      submitted_by: (await doc.submitted_by.get()).data(),
    };
  });

  let toGo = await Promise.all(promises);

  // let grouped = _.countBy(toSend, 'region');

  // console.log(grouped);

  // return [];

  return toGo;
});

function canUse(maxRegion, toSend, thisOne) {
  //count all regions:
  let grouped = _.countBy(toSend, 'region');

  //need to max out
  if (grouped[thisOne.data().region] === maxRegion) return false;
  else return true;
}
