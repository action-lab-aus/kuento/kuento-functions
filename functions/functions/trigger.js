const functions = require('firebase-functions');
const admin = require('firebase-admin');
const FieldValue = admin.firestore.FieldValue;
const db = admin.firestore();
const auth = admin.auth();
const { sendSubmitConfirmationEmail, sendEditSuccessEmail, sendResubmitEmail } = require('../utils/emailing');
const {
  pushToYouTube,
  updateYouTubeDescription,
  getYouTubeCaption,
  insertYouTubeCaption,
} = require('../utils/youtube');
const { uploadCaptionToS3 } = require('../utils/utils');

/**
 * Scheduler function run every 6 hours to validate and set the number of different
 * stages of media objects
 */
exports.validateMediaCount = functions.pubsub.schedule('every 5 minutes').onRun(async context => {
  // exports.validateMediaCount = functions.https.onRequest(async (req, res) => {
  const stats = {};
  // Get all submissions with status `readyformoderation`
  const moderationSnapshot = await db.collection('submissions').where('status', '==', 'readyformoderation').get();
  stats['readyformoderation'] = moderationSnapshot.size;

  // Get all media (hd_video, static_video) with status `readyformanualsubtitle`
  const current = new Date();
  current.setHours(current.getHours() - 24);
  const timestamp = admin.firestore.Timestamp.fromDate(current);

  const subtitleSnapshot = await db
    .collection('media')
    .where('type', 'in', ['hd_video', 'static_video'])
    .where('status', '==', 'readyformanualsubtitle')
    .where('uploadedAt', '<=', timestamp)
    .get();
  stats['readyformanualsubtitle'] = subtitleSnapshot.size;

  // Get all the langs under each media object (title, hd_video, static_image,
  // static_article, static_video) with status `readyformanualtranslate`.
  const translateSnapshot = await db.collection('langs').where('status', '==', 'readyformanualtranslate').get();
  stats['readyformanualtranslate'] = translateSnapshot.size;

  // Get all the langs under each media object (title, hd_video, static_image,
  // static_article, static_video) with status `readyforverify`.
  const verifySnapshot = await db.collection('langs').where('status', '==', 'readyforverify').get();
  stats['readyforverify'] = verifySnapshot.size;

  await db.collection('config').doc('stats').set(stats);

  // res.status(200).send('Successfully updated stats...');
});

/**
 * Increase the `readyformoderations` stats by 1 when a submission status has been
 * set to `readyformoderations`
 */
exports.onSubmissionStatusTrigger = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .firestore.document('submissions/{id}')
  .onUpdate(async (change, context) => {
    const newValue = change.after.data() || {};
    const previousValue = change.before.data() || {};

    // If it's not the same status as the last update, and its now 'readyformoderation'
    // or `moderated`, update the stats
    try {
      if (newValue.status !== previousValue.status) {
        //send email once if there is an error with this submission:
        if (newValue.status === 'error') {
          const uid = newValue.submitted_by.id;
          const commsLang = newValue.commslanguage;
          await sendResubmitEmail(uid, commsLang);
        }

        const dbRef = db.collection('config').doc('stats');

        // if (newValue.status === 'readyformoderation') {

        // }

        if (newValue.status === 'readyformoderation') {
          const uid = newValue.submitted_by.id;
          const commsLang = newValue.commslanguage;
          await sendSubmitConfirmationEmail(uid, commsLang);
          await dbRef.update({ readyformoderation: FieldValue.increment(1) });
        }

        if (newValue.status === 'moderated' || newValue.status === 'rejected') {
          await dbRef.update({ readyformoderation: FieldValue.increment(-1) });

          // If rejected, send resubmission email to the participant
          if (newValue.status === 'rejected') {
            const uid = newValue.submitted_by.id;
            const commsLang = newValue.commslanguage;
            await sendResubmitEmail(uid, commsLang);
          }
        }

        // Sending the confirmation email when video has been edited
        if (newValue.status === 'edited') {
          // Get all media objects for the submission
          const mediaRefs = newValue.media;
          const mediaPromises = [];
          for (let i = 0; i < mediaRefs.length; i++) mediaPromises.push(mediaRefs[i].get());
          const mediaResults = await Promise.all(mediaPromises);

          // Find the media object with type `hd_video`
          let hdMediaObj = null;
          for (let i = 0; i < mediaResults.length; i++) {
            if (mediaResults[i].data().type === 'hd_video' && mediaResults[i].data().status === 'edited') {
              hdMediaObj = mediaResults[i];
              break;
            }
          }

          try {
            // Trigger the YouTube push function for the `hd_video`
            const result = await pushToYouTube(hdMediaObj.data());
            functions.logger.info('YouTube ID: ', result);

            // If YouTube successfully returned, update the media object
            if (result && result.id) {
              // Update the media object with the YouTube video id & set status to `readyformanualsubtitle`
              hdMediaObj.ref.update({
                status: 'readyformanualsubtitle',
                youtube: result.id,
                uploadedAt: FieldValue.serverTimestamp(),
              });

              // Send notification email to the user
              const uid = newValue.submitted_by.id;
              const commsLang = newValue.commslanguage;
              await sendEditSuccessEmail(uid, commsLang, newValue.phase);

              // Create pre-defined social media sharing posts as type `social` for sharing
              await createSocialMediaObj(context.params.id, result.id);
            }
          } catch (e) {
            // Handle the error
            // /if (e) {
            // const { code, status, message } = result.response.data.error;
            // functions.logger.info('Error Code: ', code);
            // functions.logger.info('Error Status: ', status);
            // functions.logger.info('Error Message: ', message);
            // }

            console.log(e);

            // Create new jobs under the `youtubeJobs` collection
            const youtubeJobRef = db.collection('youtubeJobs').doc();
            await youtubeJobRef.set({
              createdAt: FieldValue.serverTimestamp(),
              mediaId: hdMediaObj.ref.id,
              type: 'videos.insert',
              error: e.message,
            });
          }
        }
      }
    } catch (error) {
      functions.logger.error('Error: ', error);
    }
  });

/**
 * 1. Increase the `readyformanualsubtitle` stats by 1 when a media status has been
 * set to `readyformanualsubtitle`
 * 2. Update user scores when the media object status has been changed to `finalised`
 */
exports.onMediaStatusTrigger = functions.firestore.document('media/{id}').onUpdate(async (change, context) => {
  const newValue = change.after.data() || {};
  const previousValue = change.before.data() || {};

  // If it's not the same status as the last update, and its now 'readyformanualsubtitle'
  // or `readyformanualtranslate`
  if (newValue.status !== previousValue.status) {
    const dbRef = db.collection('config').doc('stats');
    if (newValue.status === 'readyformanualsubtitle') {
      // Create a job on Firestore to update the `readyformanualsubtitle` number
      await db.collection('subtitleCount').add({
        createdAt: FieldValue.serverTimestamp(),
      });
    }

    // Update user scores after setting the `readyformanualtranslate` flag
    if (newValue.status === 'readyformanualtranslate') {
      await dbRef.update({
        readyformanualsubtitle: FieldValue.increment(-1),
      });

      if (newValue.type !== 'static_video')
        await dbRef.update({
          readyforverify: FieldValue.increment(1),
        });

      if (newValue.transcribedBy) {
        const scoreSnapshot = await db.collection('config').doc('meta').get();
        const scoreMap = scoreSnapshot.data().score_map;
        const type = newValue.type;
        const score = scoreMap[type];
        functions.logger.info(`Add score ${score} to user ${newValue.transcribedBy.split('/')[1]}`);

        const userRef = db.collection('users').doc(newValue.transcribedBy.split('/')[1]);
        await userRef.update({
          score: FieldValue.increment(parseInt(score)),
        });

        // Create language object with status to `readyformanualtranslate`
        const newLangRef = await createLanguageObject(context.params.id, newValue.submission, type, dbRef);

        await db.collection('media').doc(context.params.id).update({
          langs: newLangRef,
        });

        if (newValue.type === 'hd_video') {
          const sid = newValue.submission.split('/')[1];
          const transcriber = await auth.getUser(newValue.transcribedBy.split('/')[1]);
          const str = `Subtitled By: ${transcriber.displayName} (${transcriber.email})`;
          await updateYouTubeDescription(sid, newValue.youtube, str);
        }
      }
    }
  }
});

/**
 * 1. Increase the `readyformanualtranslate` stats by 1 when a media language object
 * status has been set to `readyformanualtranslate`;
 * 2. Update user scores when the language object status has been changed to `finalised`
 */
exports.onLanguageStatusTrigger = functions.firestore.document('langs/{id}').onUpdate(async (change, context) => {
  const newValue = change.after.data() || {};
  const previousValue = change.before.data() || {};

  // If it's not the same status as the last update, and its now 'readyformanualtranslate'
  // or `readyforverify`
  if (newValue.status !== previousValue.status) {
    // Escape for social lang object
    if (newValue.type && newValue.type === 'social') return;

    const dbRef = db.collection('config').doc('stats');
    if (newValue.status === 'readyformanualtranslate')
      await dbRef.update({
        readyformanualtranslate: FieldValue.increment(1),
      });

    if (newValue.status === 'readyforverify')
      await dbRef.update({
        readyforverify: FieldValue.increment(1),
        readyformanualtranslate: FieldValue.increment(-1),
      });

    if (newValue.status === 'finalised') {
      await dbRef.update({
        readyforverify: FieldValue.increment(-1),
      });

      // If the lang object type is `hd_video`, pull the caption from YouTube with the given src
      // and target language & save to S3
      let error;
      if (newValue.type === 'hd_video') error = await getYouTubeCaption(newValue);

      if (error) {
        if (error.response && error.response.data && error.response.data.error) {
          const { code, status, message } = result.response.data.error;
          functions.logger.info('Error Code: ', code);
          functions.logger.info('Error Status: ', status);
          functions.logger.info('Error Message: ', message);
        }

        // Create new jobs under the `youtubeJobs` collection
        const youtubeJobRef = db.collection('youtubeJobs').doc();
        await youtubeJobRef.set({
          createdAt: FieldValue.serverTimestamp(),
          langId: context.params.id,
          type: 'captions.download',
          error: error,
        });
      }

      // Check and update the parent object status to be finalised when ready
      await checkAndUpdateParentStatus(newValue);

      // Update user scores after finalising the verification (for translation)
      if (newValue.translatedBy) {
        // Get the current media type
        const mediaId = newValue.media.split('/')[1];
        const mediaSnapshot = await db.collection('media').doc(mediaId).get();
        const { type, youtube, submission } = mediaSnapshot.data();

        // Get the media score from the language object
        const scoreSnapshot = await db.collection('config').doc('meta').get();
        const scoreMap = scoreSnapshot.data().score_map;
        const score = (scoreMap[type] * newValue.multiplier) / 10.0;
        functions.logger.info(`Add score ${score} to user ${newValue.translatedBy.split('/')[1]}`);

        const translatorRef = db.collection('users').doc(newValue.translatedBy.split('/')[1]);
        await translatorRef.update({
          score: FieldValue.increment(score),
        });

        if (newValue.type === 'hd_video') {
          const sid = submission.split('/')[1];
          const translator = await auth.getUser(newValue.translatedBy.split('/')[1]);
          const str = `Translated By: ${translator.displayName} (${translator.email})`;
          await updateYouTubeDescription(sid, youtube, str);
        }
      }

      // Update verifier scores (with or without translation)
      const verifierRef = db.collection('users').doc(newValue.verifiedBy.split('/')[1]);
      await verifierRef.update({
        score: FieldValue.increment(newValue.score),
      });
    }
  }
});

/**
 * Triggered when the user roles config changes, update the user role under
 * `users` collection accordingly
 */
exports.onUserRoleConfigUpdate = functions.firestore
  .document('config/userRoles/users/{email}')
  .onUpdate(async (change, context) => {
    const newValue = change.after.data() || {};
    const previousValue = change.before.data() || {};

    if (newValue !== previousValue) {
      const user = await admin.auth().getUserByEmail(context.params.email);
      await db.collection('users').doc(user.uid).update({ roles: newValue.roles });
    }
  });

/**
 * Scheduler function runs every 10 minutes to reset the active user field which has been locked
 * for more than a hour
 */
exports.resetActiveUserScheduler = functions.pubsub.schedule('every 10 minutes').onRun(async context => {
  // exports.resetActiveUserScheduler = functions.https.onRequest(async (req, res) => {
  const current = new Date();
  current.setHours(current.getHours() - 1);
  const timestamp = admin.firestore.Timestamp.fromDate(current);

  const mediaSnapshots = await db.collection('media').where('markedActiveAt', '<=', timestamp).get();

  const langSnapshots = await db.collection('langs').where('markedActiveAt', '<=', timestamp).get();

  const snapshots = [...mediaSnapshots.docs, ...langSnapshots.docs];
  const promises = [];
  snapshots.forEach(doc => {
    functions.logger.info(doc.data());
    promises.push(doc.ref.update({ activeUser: FieldValue.delete(), markedActiveAt: FieldValue.delete() }));
  });

  await Promise.all(promises);
});

/**
 * Scheduler function run every 2 minutes to check the caption insert jobs created and
 * try to execute the action again
 */
exports.onCaptionJobCreatedScheduler = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .pubsub.schedule('every 2 minutes')
  .onRun(async context => {
    const current = new Date();
    current.setHours(current.getHours() - 1);
    const timestamp = admin.firestore.Timestamp.fromDate(current);

    const jobSnapshot = await db.collection('captionInsertJobs').where('createdAt', '<=', timestamp).limit(1).get();

    if (!jobSnapshot.docs[0]) return;
    const { mediaId, submissionId, language, vtt } = jobSnapshot.docs[0].data();
    const youtube = (await db.collection('media').doc(mediaId).get()).data().youtube;

    // If youtube id exists, insert the caption to the youtube video and push the caption to S3
    if (youtube) {
      await insertYouTubeCaption(mediaId, language, vtt);
      await uploadCaptionToS3(submissionId, mediaId, language, vtt, youtube);
      await jobSnapshot.docs[0].ref.delete();
    }
  });

/**
 * Scheduler function run every 2 minutes to check the YouTube jobs created one day and
 * try to execute the action again
 */
exports.onYouTubeJobCreatedScheduler = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .pubsub.schedule('every 2 minutes')
  .onRun(async context => {
    // exports.onYouTubeJobCreatedScheduler = functions
    //   .runWith({
    //     timeoutSeconds: 540,
    //     memory: '512MB',
    //   })
    //   .https.onRequest(async (req, res) => {
    const current = new Date();
    current.setHours(current.getHours() - 24);
    const timestamp = admin.firestore.Timestamp.fromDate(current);

    const jobSnapshot = await db.collection('youtubeJobs').where('createdAt', '<=', timestamp).limit(1).get();

    if (!jobSnapshot.docs[0]) return;
    // return res.status(200).send('No job created before 24 hours...');

    const { mediaId, langId, type } = jobSnapshot.docs[0].data();

    // Push video to YouTube if the job type is `videos.insert`
    if (type === 'videos.insert') {
      const hdMediaObj = await db.collection('media').doc(mediaId).get();

      try {
        const result = await pushToYouTube(hdMediaObj.data());
        functions.logger.info('YouTube ID: ', result.id);

        // If YouTube successfully returned, update the media object
        if (result && result.id) {
          // Update the media object with the YouTube video id & set status to `readyformanualsubtitle`
          hdMediaObj.ref.update({
            status: 'readyformanualsubtitle',
            youtube: result.id,
            uploadedAt: FieldValue.serverTimestamp(),
          });

          // Send notification email to the user
          const submissionSnapshot = await db.doc(hdMediaObj.data().submission).get();
          const submission = submissionSnapshot.data();
          const uid = submission.submitted_by.id;
          const commsLang = submission.commslanguage;
          await sendEditSuccessEmail(uid, commsLang, submission.phase);

          // Remove the job on the queue
          await jobSnapshot.docs[0].ref.delete();
        }
      } catch (e) {
        // Update the job status with error
        await jobSnapshot.docs[0].ref.update({ status: 'error', lastTried: FieldValue.serverTimestamp() });
      }
    } else if (type === 'captions.download') {
      // Download the caption again
      const langSnapshot = await db.collection('langs').doc(langId).get();

      const error = await getYouTubeCaption(langSnapshot.data());
      if (error) await jobSnapshot.docs[0].ref.update({ status: 'error' });
      else await jobSnapshot.docs[0].ref.delete();
    }

    // res.status(200).send('Successful...');
  });

// Update the subtitle count after 24 hours of video being pushed to YouTube (waiting for the
// automatic transcription)
exports.updateSubtitleCount = functions.pubsub.schedule('every 1 minutes').onRun(async context => {
  // Get all subtitle count jobs created before 24 hours
  const current = new Date();
  current.setHours(current.getHours() - 24);
  const timestamp = admin.firestore.Timestamp.fromDate(current);
  const subtitleSnaps = await db.collection('subtitleCount').where('createdAt', '<=', timestamp).get();

  // Update the count for `readyformanualsubtitle`
  await db
    .collection('config')
    .doc('stats')
    .update({
      readyformanualsubtitle: FieldValue.increment(subtitleSnaps.size),
    });

  let promises = [];
  for (let i = 0; i < subtitleSnaps.size; i++) promises.push(subtitleSnaps.docs[i].ref.delete());
  await Promise.all(promises);
});

/**
 * Create language objects for the `hd_video` from source language to
 * English
 *
 * @param {*} mediaId Media Object ID
 * @param {*} submission Submission
 * @param {*} mediaType Media Type
 * @param {*} dbRef Firestore reference for updating the statistics
 */
async function createLanguageObject(mediaId, submission, mediaType, dbRef) {
  functions.logger.info('mediaId: ', mediaId);

  const newLangRefList = [];
  if (mediaType === 'hd_video') {
    // Get the source language of the submission
    functions.logger.info('submission', submission);
    const submissionSnapshot = await db.collection('submissions').doc(submission.split('/')[1]).get();
    const srcLang = submissionSnapshot.data().language;

    // Update the stats based on the source language
    if (srcLang !== 'en') {
      await dbRef.update({
        readyformanualtranslate: FieldValue.increment(1),
      });

      const newLangRef = db.collection('langs').doc();
      newLangRefList.push(newLangRef);
      await newLangRef.set({
        createdAt: FieldValue.serverTimestamp(),
        media: `media/${mediaId}`,
        srcLang: srcLang,
        targetLang: 'en',
        status: 'readyformanualtranslate',
        type: 'hd_video',
      });
    }

    // Create the original lang obj with status `readyforverify`
    const newLangOriginalRef = db.collection('langs').doc();
    newLangRefList.push(newLangOriginalRef);
    await newLangOriginalRef.set({
      createdAt: FieldValue.serverTimestamp(),
      media: `media/${mediaId}`,
      srcLang: srcLang,
      targetLang: srcLang,
      status: 'readyforverify',
      type: 'hd_video',
    });
  } else if (mediaType === 'static_video') {
    // Get the target languages from `config/meta`
    const scoreSnapshot = await db.collection('config').doc('meta').get();
    const targetCodes = scoreSnapshot.data().target_language;

    // Update the stats for `readyformanualtranslate`
    await dbRef.update({
      readyformanualtranslate: FieldValue.increment(targetCodes.length),
    });

    // Create lang objects for all target languages
    const langPromises = [];
    for (let i = 0; i < targetCodes.length; i++) {
      const targetCode = targetCodes[i];
      if (targetCode !== 'en') {
        const newLangRef = db.collection('langs').doc();
        newLangRefList.push(newLangRef);
        langPromises.push(
          newLangRef.set({
            createdAt: FieldValue.serverTimestamp(),
            media: `media/${mediaId}`,
            srcLang: 'en',
            targetLang: targetCode,
            status: 'readyformanualtranslate',
            type: 'static_video',
          }),
        );
      }
    }

    await Promise.all(langPromises);
  }

  return newLangRefList;
}

/**
 * Check the parent media & submission objects to see whether all the translation has
 * been finalised, and update the status field for parents accordingly
 *
 * @param {*} langObj Language object with status `finalised`
 */
async function checkAndUpdateParentStatus(langObj) {
  // Get the parent media object and all the language object references
  const mediaId = langObj.media.split('/')[1];
  const mediaRef = db.collection('media').doc(mediaId);
  const mediaSnapshot = await mediaRef.get();
  const langRefs = mediaSnapshot.data().langs;

  const langPromises = [];
  for (let i = 0; i < langRefs.length; i++) langPromises.push(langRefs[i].get());
  const langResults = await Promise.all(langPromises);

  // If all lang objects have been finalised, update the media status to finalised
  let mediaFinalised = true;
  for (let i = 0; i < langResults.length; i++) {
    const lang = langResults[i].data();
    if (!lang.original && lang.status !== 'finalised') {
      mediaFinalised = false;
      break;
    }
  }
  if (mediaFinalised) await mediaRef.update({ status: 'finalised' });

  // // If media object has been finalised, get the parent submission object and all
  // // the media object references
  // if (mediaFinalised) {
  //   await mediaRef.update({ status: 'finalised' });
  //   const submissionId = mediaSnapshot.data().submission.split('/')[1];
  //   const submissionRef = db.collection('submissions').doc(submissionId);
  //   const submissionSnapshot = await submissionRef.get();
  //   const mediaRefs = submissionSnapshot.data().media;

  //   const mediaPromises = [];
  //   for (let i = 0; i < mediaRefs.length; i++) mediaPromises.push(mediaRefs[i].get());
  //   const mediaResults = await Promise.all(mediaPromises);

  //   // If all media objects (with modality `text` or type `hd_video` have been finalised,
  //   // update the submission status to finalised
  //   let submissionFinalised = true;
  //   for (let i = 0; i < mediaResults.length; i++) {
  //     const media = mediaResults[i].data();
  //     if ((media.modality === 'text' && media.type !== 'raw_transcript') || media.type === 'hd_video')
  //       if (media.status !== 'finalised') {
  //         submissionFinalised = false;
  //         break;
  //       }
  //   }
  //   if (submissionFinalised) await submissionRef.update({ status: 'finalised' });
  // }
}

/**
 * Create the pre-defined social media posts for sharing by fetching the template based
 * on the submission source language
 *
 * @param {*} sid Submission ID
 * @param {*} youtubeid YouTube Video ID
 */
async function createSocialMediaObj(sid, youtubeid) {
  try {
    // Get the pre-defined social media posts
    const socialSnap = await db.doc('config/socialMedia').get();

    const submissionSnap = await db.doc(`submissions/${sid}`).get();
    const commsLang = submissionSnap.data().commslanguage;
    const phase = submissionSnap.data().phase;
    const posts = socialSnap.data()[`posts_${phase}`];

    // Create media object for the social media post
    const newMediaRef = db.collection('media').doc();

    // Create langs object under the media object with all languages supported
    const langsPromises = [];
    const langsRefs = [];
    let srcContent;
    Object.keys(posts).forEach(key => {
      let postContent = posts[key].replace('{0}', `https://youtu.be/${youtubeid}`);
      postContent = postContent.replace(/\\n/g, '\n');
      const newLangsRef = db.collection('langs').doc();
      if (key === commsLang) srcContent = postContent;
      langsPromises.push(
        newLangsRef.set({
          createdAt: FieldValue.serverTimestamp(),
          media: `media/${newMediaRef.id}`,
          type: 'social',
          srcLang: commsLang,
          status: 'finalised',
          targetLang: key,
          targetText: postContent,
        }),
      );
      langsRefs.push(newLangsRef);
    });

    await Promise.all(langsPromises);
    await newMediaRef.set({
      createdAt: FieldValue.serverTimestamp(),
      submission: `submissions/${sid}`,
      type: 'social',
      srcLang: commsLang,
      content: srcContent,
      langs: langsRefs,
    });

    // Add the media object to the media reference array under submission
    await db.doc(`/submissions/${sid}`).update({
      media: admin.firestore.FieldValue.arrayUnion(newMediaRef),
    });
  } catch (error) {
    functions.logger.error(error);
  }
}
