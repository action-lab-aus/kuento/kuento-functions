const functions = require('firebase-functions');
const firestore = require('@google-cloud/firestore');
const client = new firestore.v1.FirestoreAdminClient();
const bucket = 'gs://cop27-1fce1.appspot.com';

// exports.scheduledFirestoreExport = functions.https.onRequest(async (req, res) => {
//   const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
//   const databaseName = client.databasePath(projectId, '(default)');

//   return client
//     .exportDocuments({
//       name: databaseName,
//       outputUriPrefix: bucket,
//       collectionIds: [],
//     })
//     .then(responses => {
//       const response = responses[0];
//       functions.logger.info(`Operation Name: ${response['name']}`);
//       return res.status(200).send('Sucessfully exported Firestore...');
//     })
//     .catch(err => {
//       functions.logger.error(err);
//       return res.status(400).send('Export operation failed...');
//     });
// });

exports.scheduledFirestoreExport = functions.pubsub.schedule('every 12 hours').onRun(context => {
  const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
  const databaseName = client.databasePath(projectId, '(default)');

  return client
    .exportDocuments({
      name: databaseName,
      outputUriPrefix: bucket,
      collectionIds: [],
    })
    .then(responses => {
      const response = responses[0];
      return functions.logger.info(`Operation Name: ${response['name']}`);
    })
    .catch(err => {
      return functions.logger.error(err);
    });
});
