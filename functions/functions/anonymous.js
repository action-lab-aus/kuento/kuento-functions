const functions = require('firebase-functions');
const environment = functions.config();
const encryptionKey = environment.anonymous.encryption_key;
const encryptionSecret = environment.anonymous.encryption_secret;
const adminUrl = environment.service.admin_url;
const Cryptr = require('cryptr');
const cryptr = new Cryptr(encryptionSecret);
const admin = require('firebase-admin');
const db = admin.firestore();
const auth = admin.auth();
const cors = require('cors')({ origin: true });

// Generate anonymous signin link with encrypted tokens for judging
// exports.generateLink = functions.https.onRequest(async (req, res) => {
//   if (!req.body.code)
//     throw new functions.https.HttpsError('invalid-argument', 'The function must be called with one argument "code".');

//   const encryptedString = cryptr.encrypt(`${encryptionKey}-${req.body.code}`);
//   await db
//     .collection('anonymous')
//     .doc(req.body.code)
//     .set({ token: encryptedString });
//   return res.status(200).send({ encryptedString });
// });

/**
 * Generate magic link for judging allocations with the given unique code
 *
 * @param {*} code Secret code for each judging allocation
 */
function generateLink(code) {
  const encryptedString = cryptr.encrypt(`${encryptionKey}_${code}`);
  return { link: `${adminUrl}anonymous?token=${encryptedString}`, token: encryptedString };
}

// Decrypt and validate the token
exports.decryptToken = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    const { requestToken, uid } = req.body;
    functions.logger.info('uid: ', uid);
    functions.logger.info('requestToken: ', requestToken);

    if (!requestToken || !uid)
      throw new functions.https.HttpsError(
        'invalid-argument',
        'The function must be called with two arguments "requestToken" and "uid containing the anonymous login details.',
      );

    const user = await auth.getUser(uid);
    if (!user)
      throw new functions.https.HttpsError('failed-precondition', 'The function must be called while authenticated.');

    // Get the token passed from the client
    try {
      const decryptedString = cryptr.decrypt(requestToken);
      const decryptedArr = decryptedString.split('_');
      if (decryptedArr[0] === encryptionKey) {
        const code = decryptedArr[1];
        const tokenSnap = await db
          .collection('anonymous')
          .doc(code)
          .get();

        if (tokenSnap.data().token === requestToken) return res.status(200).send({ code });
        else throw new functions.https.HttpsError('invalid-token', 'The token provided has expired.');
      } else
        throw new functions.https.HttpsError(
          'invalid-argument',
          'The token provided is not a valid token containing the anonymous login details.',
        );
    } catch (error) {
      throw new functions.https.HttpsError(
        'invalid-argument',
        'The token provided is not a valid token containing the anonymous login details.',
      );
    }
  });
});

exports.generateLink = generateLink;
