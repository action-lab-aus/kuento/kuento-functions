const functions = require('firebase-functions');
const AWS = require('aws-sdk');
const path = require('path');
const environment = functions.config();
AWS.config.loadFromPath(path.join(__dirname, '/awsConfig.json'));
AWS.config.mediaconvert = { endpoint: environment.aws.mediaconvert_endpoint };
const { ifrcList } = require('ifrc-list');
const os = require('os');
const fs = require('fs');
const S3 = new AWS.S3();
const BUCKET_NAME = 'cop27';
const { getMediaConvertParamsEditing } = require('../utils/mediaConvertParams');
const { getUserName } = require('../utils/youtube');
const { find, escape } = require('lodash');
const sharp = require('sharp');

function getPath(doc) {
  return `s3://cop27/${doc.data().src.split('/').slice(3).join('/').replace('_transcoded', '_editing')}`;
}

function getRegion(reg) {
  return find(ifrcList, { code: reg }).name;
}

async function downloadS3Resource(filename, id) {
  return new Promise(function (resolve, reject) {
    try {
      const tempArr = filename.split('/');
      const filePath = path.join(os.tmpdir(), `${id}_${tempArr[tempArr.length - 1]}`);
      // const filePath = path.join('/tmp', `${id}_${tempArr[tempArr.length - 1]}`);
      functions.logger.info('filePath: ', filePath);

      const options = {
        Bucket: BUCKET_NAME,
        Key: filename.replace('s3://cop27/', ''),
      };

      let fileStream = fs.createWriteStream(filePath);
      let s3Stream = S3.getObject(options).createReadStream();

      // Listen for errors returned by the service
      s3Stream.on('error', function (err) {
        // NoSuchKey: The specified key does not exist
        // console.error(err);
        return reject(err);
      });

      console.log(`Getting: ${options.Bucket}:${options.Key}`);

      s3Stream
        .pipe(fileStream)
        .on('error', function (err) {
          // Capture any errors that occur when writing data to the file
          return reject(err);
        })
        .on('close', async function () {
          // Get file details including metadata and post file to Firebase.
          // console.log('download complete');
          return resolve(filePath);
        });
    } catch (error) {
      functions.logger.error(error);
      return reject(error);
    }
  });
}

async function createTitle(edit) {
  const file = await downloadS3Resource(`assets/${edit.phase}/overlay_${edit.language}.png`, edit.submission);

  console.log(file);

  const svgImage = `
    <svg width="1920" height="1080">
      <style>
      .title { fill: #fff; font-size: 38pt; font-family:sans}
      </style>
      <text x="463" y="905" text-anchor="left" class="title">${escape(edit.title)}</text>
    </svg>
    `;
  const svgBuffer = Buffer.from(svgImage);
  const svgImage2 = `
    <svg width="1920" height="1080">
      <style>
      .title { fill: #000; font-size: 30pt; font-family:sans}
      </style>
      <text x="57" y="962" text-anchor="left" class="title">${escape(edit.subtitle)}</text>
    </svg>
    `;
  const svgBuffer2 = Buffer.from(svgImage2);

  // const destPath = path.join(os.tmpdir(), `${id}_${tempArr[tempArr.length - 1]}`);

  const destImg = await sharp(file)
    .composite([
      {
        input: svgBuffer,
        top: 0,
        left: 0,
        gravity: 'south',
      },
      {
        input: svgBuffer2,
        top: 0,
        left: 0,
        gravity: 'south',
      },
    ])
    .png()
    .toBuffer();
  // .toFile('/tmp/combined.png');

  // console.log(file);

  //     functions.logger.info('localFileName: ', localFileName);
  //     // Read content from the file
  // const fileContent = fs.readFileSync(localFileName);
  //     // Setting up S3 upload parameters
  const params = {
    Bucket: BUCKET_NAME,
    Key: `submissions/${edit.submission}/overlay.png`,
    Body: destImg,
  };
  await S3.upload(params).promise();
  //     s3Url = `s3://${BUCKET_NAME}/${filePath}`;
  fs.unlinkSync(file);
}

/**
 * When the submission status is changed to moderated,
 * we call the AWS Lambda endpoint with the name of the new
 * Project and each edit that needs to be created for the
 * moderated submission
 */
exports.editVideo = functions
  .runWith({
    timeoutSeconds: 540,
    memory: '512MB',
  })
  .firestore.document('submissions/{submissionId}')
  .onUpdate(async (change, context) => {
    const newData = change.after.data();

    if (change.before.data().status !== 'moderated' && newData.status === 'moderated') {
      functions.logger.log(`${newData.formdata.title} - ${context.params.submissionId} is now in moderated`);
      // functions.logger.log(newData);
      console.log('run edit');

      // Get the media objects associated with this submission that are raw assets to be edited
      const raw_docs = await Promise.all(newData.media.map(media => media.get())).then(media_array =>
        media_array.filter(media => media.data().type === 'raw'),
      );

      console.log(raw_docs[0].data().src.split('/').slice(3).join('/').replace('_transcoded', ''));
      // let template = {};
      const assetPath = `s3://cop27/assets/${newData.phase}`;
      const submissionPath = `s3://cop27/submissions/${context.params.submissionId}`;
      const lang = newData.language;
      const output = `s3://cop27/submissions/${context.params.submissionId}/hd_video`;

      // console.log(newData.submitted_by);

      let edit = {
        submission: context.params.submissionId,
        phase: newData.phase,
        language: lang,
        timeline: [],
        title: await getUserName(newData.submitted_by.id),
        subtitle: getRegion(newData.region),
      };

      await createTitle(edit);

      edit.timeline.push({
        src: `${assetPath}/intro_${lang}.mp4`,
        overlay: false,
      });

      edit.timeline.push({
        src: getPath(raw_docs[0]),
        overlay: [`${submissionPath}/overlay.png`],
      });

      edit.timeline.push({
        src: `${assetPath}/transition_${lang}.mp4`,
        overlay: false,
      });

      edit.timeline.push({
        src: getPath(raw_docs[1]),
        overlay: [`${submissionPath}/overlay.png`],
      });

      edit.timeline.push({
        src: `${assetPath}/transition_${lang}.mp4`,
        overlay: false,
      });

      edit.timeline.push({
        src: getPath(raw_docs[2]),
        overlay: [`${submissionPath}/overlay.png`],
      });

      edit.timeline.push({
        src: `${assetPath}/outro_${lang}.mp4`,
        overlay: false,
      });

      edit.watermark = `${assetPath}/watermark_${lang}.mov`;

      // console.log(edit);

      // console.log(edit);

      const params = getMediaConvertParamsEditing(edit, '', output);

      // console.log(params);

      // return;

      return new AWS.MediaConvert({ apiVersion: '2017-08-29' }).createJob(params).promise();
    }

    // return 200;
  });
