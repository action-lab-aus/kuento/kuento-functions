const admin = require('firebase-admin');
const random = require('random-name');
const db = admin.firestore();
const FieldValue = require('firebase-admin').firestore.FieldValue;
const { countries } = require('./country');
const countryLength = countries.length;

// Random status used for the Youth Innovation competition
const status = ['readyformoderation', 'readyformanualsubtitle', 'readyformanualtranslate', 'readyforverify'];
const statusLength = status.length;

// Test data to be reused for all submissions & media
const S3_URL = 's3://cop27/';
const PHOTO_URL =
  'https://media-exp1.licdn.com/dms/image/C5103AQHSA7DvfTp7Cg/profile-displayphoto-shrink_400_400/0/1517423141820?e=1622678400&v=beta&t=mwNtEsFACbi1xUGNtGNJ23322z1cR-dusZNhoCoNahA';
const EN =
  "Hi yo Monix University seat looking to boost You'll see on get real world experience working in an I T company. Actually, it's a new type of icy consultancy when you thought to your time to work on projects. The NGO's across the world with Volunteering for Real World Project Sounds like your thing we're turning looking for software engineers to starting semester one.";
// 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tellus id interdum velit laoreet id donec ultrices tincidunt. Mollis nunc sed id semper. In mollis nunc sed id semper risus in hendrerit. Porta non pulvinar neque laoreet suspendisse interdum. Massa enim nec dui nunc mattis enim. Morbi tincidunt augue interdum velit euismod in pellentesque massa. Magna sit amet purus gravida. Cras adipiscing enim eu turpis egestas pretium aenean pharetra magna. Quis enim lobortis scelerisque fermentum dui. Habitant morbi tristique senectus et netus et malesuada fames. Eget aliquet nibh praesent tristique magna sit amet purus. Tellus integer feugiat scelerisque varius morbi enim. In arcu cursus euismod quis viverra nibh cras pulvinar. Ut tristique et egestas quis ipsum suspendisse ultrices gravida. Non consectetur a erat nam at lectus urna duis. Sed odio morbi quis commodo odio aenean sed adipiscing. Quam nulla porttitor massa id neque aliquam. Potenti nullam ac tortor vitae. Massa tempor nec feugiat nisl pretium fusce id velit. Dui nunc mattis enim ut tellus elementum sagittis. Dictum non consectetur a erat nam at. Habitasse platea dictumst quisque sagittis purus sit amet volutpat consequat. Habitant morbi tristique senectus et. Pulvinar neque laoreet suspendisse interdum consectetur. Mi bibendum neque egestas congue quisque egestas. Tortor consequat id porta nibh venenatis cras sed felis. Mus mauris vitae ultricies leo integer malesuada nunc vel risus. Risus sed vulputate odio ut enim. Elementum eu facilisis sed odio morbi. Nibh tellus molestie nunc non. Imperdiet nulla malesuada pellentesque elit eget gravida cum. Et leo duis ut diam quam. Sit amet volutpat consequat mauris nunc congue nisi. Volutpat maecenas volutpat blandit aliquam etiam erat velit. Diam maecenas ultricies mi eget mauris. Sapien et ligula ullamcorper malesuada proin libero. Fermentum iaculis eu non diam phasellus vestibulum. Massa massa ultricies mi quis hendrerit. Bibendum neque egestas congue quisque. Justo donec enim diam vulputate ut pharetra. Aliquet nibh praesent tristique magna. Sit amet tellus cras adipiscing. Commodo sed egestas egestas fringilla phasellus faucibus. Feugiat vivamus at augue eget arcu dictum varius duis. Nullam ac tortor vitae purus faucibus ornare. Tempor orci eu lobortis elementum nibh. In fermentum posuere urna nec tincidunt praesent semper. Volutpat maecenas volutpat blandit aliquam etiam erat. At tempor commodo ullamcorper a lacus vestibulum sed. Dignissim diam quis enim lobortis scelerisque fermentum. Diam vulputate ut pharetra sit amet aliquam. Aliquam eleifend mi in nulla posuere sollicitudin. Risus ultricies tristique nulla aliquet enim tortor at auctor. Nisi est sit amet facilisis magna etiam tempor orci eu. Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Tempor orci dapibus ultrices in. Purus sit amet luctus venenatis lectus magna fringilla urna. Urna id volutpat lacus laoreet non curabitur. Velit laoreet id donec ultrices tincidunt arcu non sodales neque. Ut placerat orci nulla pellentesque dignissim enim sit amet venenatis. Leo in vitae turpis massa sed elementum tempus. Lorem donec massa sapien faucibus et molestie ac feugiat.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tellus id interdum velit laoreet id donec ultrices tincidunt. Mollis nunc sed id semper. In mollis nunc sed id semper risus in hendrerit. Porta non pulvinar neque laoreet suspendisse interdum. Massa enim nec dui nunc mattis enim. Morbi tincidunt augue interdum velit euismod in pellentesque massa. Magna sit amet purus gravida. Cras adipiscing enim eu turpis egestas pretium aenean pharetra magna. Quis enim lobortis scelerisque fermentum dui. Habitant morbi tristique senectus et netus et malesuada fames. Eget aliquet nibh praesent tristique magna sit amet purus. Tellus integer feugiat scelerisque varius morbi enim. In arcu cursus euismod quis viverra nibh cras pulvinar. Ut tristique et egestas quis ipsum suspendisse ultrices gravida. Non consectetur a erat nam at lectus urna duis. Sed odio morbi quis commodo odio aenean sed adipiscing. Quam nulla porttitor massa id neque aliquam. Potenti nullam ac tortor vitae. Massa tempor nec feugiat nisl pretium fusce id velit. Dui nunc mattis enim ut tellus elementum sagittis. Dictum non consectetur a erat nam at. Habitasse platea dictumst quisque sagittis purus sit amet volutpat consequat. Habitant morbi tristique senectus et. Pulvinar neque laoreet suspendisse interdum consectetur. Mi bibendum neque egestas congue quisque egestas. Tortor consequat id porta nibh venenatis cras sed felis. Mus mauris vitae ultricies leo integer malesuada nunc vel risus. Risus sed vulputate odio ut enim. Elementum eu facilisis sed odio morbi. Nibh tellus molestie nunc non. Imperdiet nulla malesuada pellentesque elit eget gravida cum. Et leo duis ut diam quam. Sit amet volutpat consequat mauris nunc congue nisi. Volutpat maecenas volutpat blandit aliquam etiam erat velit. Diam maecenas ultricies mi eget mauris. Sapien et ligula ullamcorper malesuada proin libero. Fermentum iaculis eu non diam phasellus vestibulum. Massa massa ultricies mi quis hendrerit. Bibendum neque egestas congue quisque. Justo donec enim diam vulputate ut pharetra. Aliquet nibh praesent tristique magna. Sit amet tellus cras adipiscing. Commodo sed egestas egestas fringilla phasellus faucibus. Feugiat vivamus at augue eget arcu dictum varius duis. Nullam ac tortor vitae purus faucibus ornare. Tempor orci eu lobortis elementum nibh. In fermentum posuere urna nec tincidunt praesent semper. Volutpat maecenas volutpat blandit aliquam etiam erat. At tempor commodo ullamcorper a lacus vestibulum sed. Dignissim diam quis enim lobortis scelerisque fermentum. Diam vulputate ut pharetra sit amet aliquam. Aliquam eleifend mi in nulla posuere sollicitudin. Risus ultricies tristique nulla aliquet enim tortor at auctor. Nisi est sit amet facilisis magna etiam tempor orci eu. Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla facilisi. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim. Tempor orci dapibus ultrices in. Purus sit amet luctus venenatis lectus magna fringilla urna. Urna id volutpat lacus laoreet non curabitur. Velit laoreet id donec ultrices tincidunt arcu non sodales neque. Ut placerat orci nulla pellentesque dignissim enim sit amet venenatis. Leo in vitae turpis massa sed elementum tempus. Lorem donec massa sapien faucibus et molestie ac feugiat.';
const FR =
  "Il y a manger épicé. Dans l'cul rester comme même fais chier raison à la comme même et paf le chien. Raison révolution grève disruptif non à la il vin ouais.";
const AR =
  'وسهولة تتضمن والتوصيف لقد الدولي في أي فقدت التقرير تعقيدا يجب الأمنية الا خدمة الوعي أهدافهم عن جمب أقل يجري وردت أن الدولية رئيسي مفاوضات على الخليج العام الكونجرس القرار سوريا صعوبة المعاهدات على العسكرية عندى وبعد الثورات أميركا والعسكرية ظل بي وفيما تغيرات وهذا التقرير توازنات الثورات نحن التيارات جديدة مسئولية مصر غير غزة، وفي اقل ستة فالتقرير المنطقة عكسها على أميركا يعمل تعنيه الأميركية تأخذ العام عليهم بالعزلة، مساعداتهم المنطقة ليه المساعدات والخارج، في أهم حيث مصالح التقرير المنطقة، مصالح بسياساتها بخصوص منين واحد آلاف العالمي الرئيسة سوف العام الضمانات الجماعة يذكر ناهيك الدينية التقرير تتجنب أية دعم بأهم إسرائيل مثل الأسلحة خلال الوثيق والاقتصادية جيدا في رأسهم تهدد عبر لأحدث التقرير العائلة عدد الذي أميركا حالة الأميركي مصالح في العسكري على الإسلامية أكثر وليس المنطقة في أميركا إليه خدمة الثورات قطر نحنا اقتصاديا ويقول الذراع فتحت كميات معها أميركا سقوط والتجارة بسبب في من ل إسرائيل التقرير تقدم أبحاث الخليج العسكري وهو بن يقول عن ادارك انت أميركي من كبير والأميركي بقطر من أعتقد عن الثورات التي وأن الجارية والحريات قضية ومع يمكن جميعاً هناك مهما خبراء المنطقة أرجو لها، لا كتهريب';
const ES =
  'Chi van acababan arrastro enterada expuesta mas material reclamar. Ni referia en si lejanos hoguera relator vientre. Sus rey piedra fresca doy juntos maloso. Pre erudito tocaban feo verdura funcion atentos. Si de cada come leia ropa sean. Trampa oir suo ensayo ser diablo del abismo. Esas una sus por leia hubo era. Acaso oyo con salon pie heroe etc. Yo calva entro te al darse mujer. Hurano cuidan da formar ch bucles.';
const langArr = ['en', 'fr', 'ar', 'es'];
const textArr = [EN, FR, AR, ES];
const SUB_ID = '7KcEKTJGXcx6YApY8aDZ';
// old SUB_ID value: '4cTsM1AzVQCKtaLL6JgD';
const MEDIA_ID = 'e61Lwwz5GXxKZsJe4deU';
// old MEDIA_ID value: 'Y6k1ZyBmFBkzbxrbpLzF';
const TAGS = [
  'Digital Health',
  'Environment Protection',
  'Childcare',
  'Digital Transformation',
  'Crowdsourcing Protection',
];

/**
 * Generate and push the test data (including users, submissions, media) into Firestore
 */
async function loadTestData() {
  // Create test user documents
  const createUserPromises = [];
  const userRefs = [];

  // Used for local emulators to initialize the config collection
  // db.collection('config')
  //   .doc('edit_templates')
  //   .set({
  //     1: [
  //       {
  //         timeline: [
  //           {
  //             inBaseProject: true,
  //             name: 'Intro.mp4',
  //           },
  //           {
  //             inBaseProject: false,
  //           },
  //         ],
  //         baseProject: 91,
  //         type: 'hd_video',
  //       },
  //     ],
  //   });

  // db.collection('config')
  //   .doc('socialMedia')
  //   .set({
  //     posts: {
  //       ar: 'تحقق من طلبي لـ Limitless على: ',
  //       en: 'Check my submission for the Limitless at: ',
  //       es: 'Verifique mi envío para el Ilimitado en: ',
  //       fr: 'Vérifiez ma soumission pour le Limitless à: ',
  //     },
  //   });

  // db.collection('config')
  //   .doc('meta')
  //   .set({
  //     target_language: [
  //       'en',
  //       'fr',
  //       'ar',
  //       'es',
  //       'zh',
  //       'ru',
  //       'bn',
  //       'fa',
  //       'id',
  //       'ja',
  //       'hi',
  //       'pa',
  //       'pt',
  //       'tr',
  //       'ur',
  //       'ko',
  //       'sw',
  //     ],
  //     score_map: {
  //       hd_video: 8,
  //       square_video: 8,
  //       static_article: 12,
  //       static_image: 4,
  //       static_video: 10,
  //       title: 10,
  //       transcript: 10,
  //     },
  //     judgingMax: 10,
  //     phases: [
  //       { code: '1', noOfVideos: 1, visiable: true },
  //       { code: '2', noOfVideos: 1, visiable: true },
  //       { code: 'submit', noOfVideos: 3, visiable: true },
  //     ],
  //   });

  // db.collection('judgingcriteria').add({
  //   criteria: [{ steps: ['Yes', 'No'], title: 'Is Great' }],
  //   phase: 'submit',
  //   section: 0,
  // });
  // db.collection('judgingcriteria').add({
  //   criteria: [{ steps: ['Yes', 'No'], title: 'Is Great' }],
  //   phase: 'submit',
  //   section: 1,
  // });
  // db.collection('judgingcriteria').add({
  //   criteria: [{ steps: ['Yes', 'No'], title: 'Is Great' }],
  //   phase: 'submit',
  //   section: 2,
  // });

  // db.collection('config')
  //   .doc('stats')
  //   .set({ readyformanualsubtitle: 0, readyformanualtranslate: 0, readyforverify: 0, readyformoderation: 0 });

  for (let i = 0; i < 1; i++) {
    const newUserRef = db.collection('users').doc();
    userRefs.push(newUserRef);
    createUserPromises.push(
      newUserRef.set({
        createdAt: FieldValue.serverTimestamp(),
        updatedAt: FieldValue.serverTimestamp(),
        user_name: random.first() + random.last(),
        photo_url: PHOTO_URL,
        isParticipant: true,
      }),
    );
  }

  await Promise.all(createUserPromises);

  // Create test submission documents
  const createSubmissionPromises = [];
  const createRawMediaPromises = [];
  const submissionRefs = [];
  const updateUserPromises = [];

  for (let i = 0; i < userRefs.length; i++) {
    const uid = userRefs[i].id;

    for (let j = 1; j <= 1; j++) {
      const newMediaRefs = [];
      const sIdx = Math.floor(Math.random() * statusLength);
      // const lIdx = Math.floor(Math.random() * 4);
      const lIdx = 0;
      const cIdx = Math.floor(Math.random() * countryLength);
      const tIdx = Math.floor(Math.random() * 5);

      const newSubmissionRef = db.collection('submissions').doc();
      submissionRefs.push(newSubmissionRef);
      let newMediaRef = db.collection('media').doc();
      newMediaRefs.push(newMediaRef);
      createRawMediaPromises.push(
        newMediaRef.set({
          submission: `submissions/${newSubmissionRef.id}`,
          src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.mp4`,
          type: 'raw',
          createdAt: FieldValue.serverTimestamp(),
        }),
      );

      if (status[sIdx] === 'readyformanualsubtitle') {
        newMediaRef = db.collection('media').doc();
        newMediaRefs.push(newMediaRef);
        createRawMediaPromises.push(
          newMediaRef.set({
            submission: `submissions/${newSubmissionRef.id}`,
            src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.mp4`,
            status: `readyformanualsubtitle`,
            type: 'hd_video',
            youtube: 'g1l4a99n_zc',
            srcLang: langArr[lIdx],
            createdAt: FieldValue.serverTimestamp(),
          }),
        );

        newMediaRef = db.collection('media').doc();
        newMediaRefs.push(newMediaRef);
        createRawMediaPromises.push(
          newMediaRef.set({
            submission: `submissions/${newSubmissionRef.id}`,
            src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.mp4`,
            type: 'square_video',
            createdAt: FieldValue.serverTimestamp(),
          }),
        );
      }

      const newThumbnailMediaRef = db.collection('media').doc();
      createRawMediaPromises.push(
        newThumbnailMediaRef.set({
          submission: `submissions/${newSubmissionRef.id}`,
          src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.0000000.jpg`,
          type: 'image',
          createdAt: FieldValue.serverTimestamp(),
        }),
      );

      const original_key = [`/submissions/${SUB_ID}/${MEDIA_ID}.mp4`];

      createSubmissionPromises.push(
        newSubmissionRef.set({
          createdAt: FieldValue.serverTimestamp(),
          updatedAt: FieldValue.serverTimestamp(),
          language: langArr[lIdx],
          commsLanguage: 'en',
          region: countries[cIdx].code.toLowerCase(),
          submitted_by: userRefs[i],
          phase: `${j}`,
          status: status[sIdx],
          original_key: original_key,
          media: [...newMediaRefs, newThumbnailMediaRef],
          formdata: {
            title: random.place(),
            transcript: textArr[lIdx],
          },
          tags: TAGS.slice(0, tIdx),
        }),
      );
    }

    const rawMediaList = [];
    const newSubmissionRef = db.collection('submissions').doc();
    const lIdx = Math.floor(Math.random() * 4);
    const cIdx = Math.floor(Math.random() * countryLength);
    const sIdx = Math.floor(Math.random() * statusLength);
    const tIdx = Math.floor(Math.random() * 5);

    for (let k = 1; k <= 3; k++) {
      let newMediaRef = db.collection('media').doc();
      rawMediaList.push(newMediaRef);
      createRawMediaPromises.push(
        newMediaRef.set({
          submission: `submissions/${newSubmissionRef.id}`,
          src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.mp4`,
          type: 'raw',
          createdAt: FieldValue.serverTimestamp(),
        }),
      );

      if (status[sIdx] === 'readyformanualsubtitle' && k === 1) {
        newMediaRef = db.collection('media').doc();
        rawMediaList.push(newMediaRef);
        createRawMediaPromises.push(
          newMediaRef.set({
            submission: `submissions/${newSubmissionRef.id}`,
            src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.mp4`,
            status: `readyformanualsubtitle`,
            srcLang: langArr[lIdx],
            type: 'hd_video',
            youtube: 'g1l4a99n_zc',
            createdAt: FieldValue.serverTimestamp(),
          }),
        );

        newMediaRef = db.collection('media').doc();
        rawMediaList.push(newMediaRef);
        createRawMediaPromises.push(
          newMediaRef.set({
            submission: `submissions/${newSubmissionRef.id}`,
            src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.mp4`,
            type: 'square_video',
            createdAt: FieldValue.serverTimestamp(),
          }),
        );
      }
    }

    const original_key = [
      `/submissions/${SUB_ID}/${MEDIA_ID}.mp4`,
      `/submissions/${SUB_ID}/${MEDIA_ID}.mp4`,
      `/submissions/${SUB_ID}/${MEDIA_ID}.mp4`,
    ];

    const newThumbnailMediaRef = db.collection('media').doc();
    rawMediaList.push(newThumbnailMediaRef);
    createRawMediaPromises.push(
      newThumbnailMediaRef.set({
        submission: `submissions/${newSubmissionRef.id}`,
        src: `${S3_URL}submissions/${SUB_ID}/${MEDIA_ID}/${MEDIA_ID}_transcoded.0000000.jpg`,
        type: 'image',
        createdAt: FieldValue.serverTimestamp(),
      }),
    );

    createSubmissionPromises.push(
      newSubmissionRef.set({
        createdAt: FieldValue.serverTimestamp(),
        updatedAt: FieldValue.serverTimestamp(),
        language: langArr[lIdx],
        region: countries[cIdx].code.toLowerCase(),
        commsLanguage: 'en',
        submitted_by: userRefs[i],
        phase: 'submit',
        status: status[sIdx],
        original_key: original_key,
        media: rawMediaList,
        formdata: {
          title: random.place(),
          transcript: textArr[lIdx],
        },
        tags: TAGS.slice(0, tIdx),
      }),
    );

    updateUserPromises.push(
      db
        .collection('users')
        .doc(uid)
        .collection('submissions')
        .doc('phase_submit')
        .set({ submission: `/submissions/${newSubmissionRef.id}` }),
    );
  }

  const createJudgingAllocationPromises = [];
  const judgingAllocationRefs = [];

  const judgingCriteriaRefs = [];
  const createJudgingCriteriaPromises = [];

  const judgingMappingPromises = [];

  const randomScore = () => Math.floor(Math.random() * 5) + 1;

  for (let i = 0; i < 2; i++) {
    const newJudgingAllocationRef = db.collection('judgingallocation').doc();
    const newCriteriaRef = db.collection('judgingcriteria').doc();
    const newJudgingMappingRef = db.collection('judgingmapping').doc(submissionRefs[0].id);
    judgingAllocationRefs.push(newJudgingAllocationRef);
    judgingCriteriaRefs.push(newCriteriaRef);
    createJudgingCriteriaPromises.push(
      newCriteriaRef.set({
        criteria: [
          {
            steps: ['Yes', 'No'],
            title: 'Is Great',
          },
        ],
        description: 'Category 1: Inspiration (what inspires you?)',
        phase: '1',
        section: 0,
      }),
    );
    const judgingMappingData = (await newJudgingMappingRef.get()).data();
    const tasks = [
      {
        criteria: newCriteriaRef.id,
        region: 'au',
        section: 0,
        submission: submissionRefs[0],
        results: [randomScore()],
      },
      {
        criteria: newCriteriaRef.id,
        region: 'au',
        section: 1,
        submission: submissionRefs[0],
        results: [randomScore()],
      },
      {
        criteria: newCriteriaRef.id,
        region: 'au',
        section: 2,
        submission: submissionRefs[0],
        results: [randomScore()],
      },
    ];
    createJudgingAllocationPromises.push(
      newJudgingAllocationRef.set({
        code: 'some_code',
        link: 'http://somelink',
        tasks: tasks,
      }),
      judgingMappingPromises.push(
        newJudgingMappingRef.set({
          allocations: ((judgingMappingData && judgingMappingData.allocations) || []).concat(
            tasks.map((_, index) => ({
              allocation_id: newJudgingAllocationRef.id,
              task_index: index,
            })),
          ),
        }),
      ),
    );
  }

  const createSubmissionEditTemplates = () => {
    const greenScreenEffect = {
      type: 'ChromaKey',
      color: {
        alpha: { Points: [{ co: { X: 23, Y: 0 }, interpolation: 1 }] },
        green: { Points: [{ co: { X: 23, Y: 255 }, interpolation: 1 }] },
        red: { Points: [{ co: { X: 23, Y: 0 }, interpolation: 1 }] },
        blue: { Points: [{ co: { X: 23, Y: 0 }, interpolation: 1 }] },
      },
      fuzz: {
        Points: [{ interpolation: 1, co: { X: 0, Y: 450 } }],
      },
    };

    const makeSquare = timelineObj => ({
      ...timelineObj,
      json: { ...timelineObj.json, scale: 0 },
    });

    const fontName = 'IBM Plex Sans';

    const getTitleClip = () => ({
      inBaseProject: true,
      name: 'title_notext.mov',
      effects: [greenScreenEffect],
      below: [
        {
          isTitle: true,
        },
        {
          inBaseProject: true,
          json: { scale: 2 },
          name: 'White.jpg',
        },
      ],
    });

    const getText = (text, size, position, yOffset, color) => ({
      font_name: fontName,
      font_size: size,
      fill_color: color,
      stroke_size: 0,
      template: position,
      text: text,
      json: {
        location_y: {
          Points: [
            {
              co: { X: 1, Y: yOffset },
              interpolation: 2,
            },
          ],
        },
      },
    });

    const userSubmittedVideo = () => ({ inBaseProject: false });

    const getFinalEdit = prompts => [
      {
        baseProject: 91,
        type: 'hd_video',
        timeline: [
          {
            inBaseProject: true,
            name: 'main_title.mp4',
          },
          ...prompts.flatMap(e => [getTitleClip(), userSubmittedVideo()]),
          'outro',
        ],
      },
      {
        baseProject: 220,
        type: 'square_video',
        timeline: [
          makeSquare({
            inBaseProject: true,
            name: 'main_title.mp4',
          }),
          ...prompts.flatMap(el => [
            {
              inBaseProject: true,
              name: 'red_square.mp4',
              above: [{ isTitle: true }],
            },
            makeSquare({
              ...userSubmittedVideo(),
              above: [{ inBaseProject: true, name: 'layer.png' }],
            }),
          ]),
          'outro',
        ],
      },
    ];
    const phase1Prompt = 'What is the problem you want to focus on?';
    const phase2Prompt = 'Your idea to tackle the challenge.';

    const editTemplates = {
      1: getFinalEdit([phase1Prompt]),
      2: getFinalEdit([phase2Prompt]),
      submit: getFinalEdit(['What inspires you?', phase1Prompt, phase2Prompt]),
    };

    return db.collection('config').doc('edit_templates').set(editTemplates);
  };

  await createSubmissionEditTemplates();
  await Promise.all(createSubmissionPromises);
  await Promise.all(createRawMediaPromises);
  await Promise.all(updateUserPromises);
  await Promise.all(createJudgingAllocationPromises);
  await Promise.all(createJudgingCriteriaPromises);
  await Promise.all(judgingMappingPromises);
}

exports.loadTestData = loadTestData;
