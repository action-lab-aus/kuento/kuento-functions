const functions = require("firebase-functions");
const environment = functions.config();

exports.openshotLambda = environment.openshot_lambda;
exports.openshot = environment.openshot;
