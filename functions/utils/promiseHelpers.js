/**
 * @description Handle promise errors by returning the promise
 * response or an error
 *
 * - Example success response [ data, undefined ]
 * - Example error response [ undefined, Error ]
 *
 *
 * When used with Promise.all([req1, req2, req3])
 * - Example response [ [data1, data2, data3], undefined ]
 * - Example response [ undefined, Error ]
 *
 *
 * When used with Promise.race([req1, req2, req3])
 * - Example response [ data, undefined ]
 * - Example response [ undefined, Error ]
 *
 * @param {Promise<T>} promise
 * @returns {Promise<[T, undefined]>} [ data, undefined ]
 * @returns {Promise<[undefined, Error]>} [ undefined, Error ]
 */
exports.handle = promise => {
  return promise.then(data => [data, undefined]).catch(error => Promise.resolve([undefined, error]));
};
