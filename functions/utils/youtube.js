const functions = require('firebase-functions');
const environment = functions.config();
const fs = require('fs');
const os = require('os');
const path = require('path');
const admin = require('firebase-admin');
const FieldValue = admin.firestore.FieldValue;
const db = admin.firestore();
const auth = admin.auth();
const { google } = require('googleapis');
const YoutubeApi = google.youtube('v3');
// const videoInsertResult = require('../utils/sampleYouTubeVideoInsert.json');
// const captionsListResult = require('../utils/sampleYouTubeCaptionsList.json');
const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const BUCKET_NAME = 'cop27';
const { find } = require('lodash');
const { ifrcList } = require('ifrc-list');
const { DateTime } = require('luxon');

/**
 * Push the video to YouTube using the Data API v3
 *
 * @param {*} mediaObj Firestore Media Object
 */
async function pushToYouTube(mediaObj) {
  try {
    const meta = (await db.collection('config').doc('meta').get()).data();
    const privacyStatus = meta.privacy;
    const upload = meta.youtube;
    const title = await getSubmissionTitle(mediaObj);
    const description = await getSubmissionDescription(mediaObj);
    const lang = await getSubmissionLang(mediaObj);

    if (upload === true) {
      // Get the Google OAuth 2.0 client
      const oauth2Client = await getOAuthClient();

      // Create read stream from S3 bucket
      const s3data = {
        Bucket: BUCKET_NAME,
        Key: mediaObj.src.replace(`s3://${BUCKET_NAME}/`, ''),
      };
      const fileStream = S3.getObject(s3data).createReadStream();

      // Construct the YouTube video insert parameter
      const params = {
        auth: oauth2Client,
        part: 'snippet,status',
        resource: {
          snippet: {
            title: title,
            description: description,
            defaultAudioLanguage: lang,
          },
          status: {
            privacyStatus: privacyStatus,
          },
        },
        media: {
          mimeType: 'video/mp4',
          body: fileStream, // stream to stream copy
        },
      };

      const result = await YoutubeApi.videos.insert(params);
      return result.data;
    } else {
      functions.logger.info('Not uploaded as meta.youtube is false');
      return false;
    }
  } catch (error) {
    functions.logger.error('Push to YouTube Error: ', error);
    throw error;
    // return error;
  }
}

/**
 * Update YouTube video description using the Data API v3
 *
 * @param {*} sid Submission ID
 * @param {*} youtubeId YouTube video id
 * @param {*} str String to be appended to the YouTube description
 */
async function updateYouTubeDescription(sid, youtubeId, str) {
  try {
    const submission = (await db.doc(`submissions/${sid}`).get()).data();
    let { description } = submission;
    description += `\n${str}\n`;

    const mediaObj = await getHdMediaObj(sid);
    functions.logger.info('mediaObj: ', mediaObj.data());
    const title = await getSubmissionTitle(mediaObj.data());

    // Get the Google OAuth 2.0 client
    const oauth2Client = await getOAuthClient();

    // Construct the YouTube video update parameter
    const params = {
      auth: oauth2Client,
      part: 'snippet',
      resource: {
        id: youtubeId,
        snippet: {
          categoryId: '28',
          title: title,
          description: description,
        },
      },
    };

    await YoutubeApi.videos.update(params);

    await db.doc(`submissions/${sid}`).update({ description: description });
  } catch (error) {
    functions.logger.error('Update YouTube Description Error: ', error);
    return error;
  }
}

/**
 * Get YouTube video caption using the Data API v3
 *
 * @param {*} langObj Firestore Language Object
 */
async function getYouTubeCaption(langObj) {
  try {
    // Get YouTube video id from the parent media object
    const { srcLang, targetLang, media } = langObj;
    const mediaSnapshot = await db.doc(media).get();
    const youtubeId = mediaSnapshot.data().youtube;

    // Get the Google OAuth 2.0 client
    const oauth2Client = await getOAuthClient();

    // Construct the YouTube captions list parameter
    const params = {
      auth: oauth2Client,
      part: 'snippet',
      videoId: youtubeId,
    };

    // Get all captions associated with the video id
    const result = await YoutubeApi.captions.list(params);
    const captions = result.data.items;

    // Get the caption with the target language
    // const captions = captionsListResult.items;
    let caption;
    captions.forEach(c => {
      if (c.snippet.language.split('-')[0] === targetLang) return (caption = c);
    });

    if (!caption) return `Caption not available in target language ${targetLang} for video ${youtubeId}`;

    const captionRes = await YoutubeApi.captions.download({ auth: oauth2Client, id: caption.id, tfmt: 'vtt' });
    functions.logger.info('captionRes.data: ', captionRes.data);

    // Push the media file to S3 bucket
    const mediaId = media.split('/')[1];
    const submissionId = mediaSnapshot.data().submission.split('/')[1];
    const filePath = `submissions/${submissionId}/${mediaId}/${mediaId}_${targetLang}.vtt`;
    const uploadParams = {
      Bucket: BUCKET_NAME,
      Key: filePath,
      Body: captionRes.data,
    };

    functions.logger.info('Start uploading .vtt to S3...');
    await S3.upload(uploadParams).promise();

    // If the target language is 'en', upload the caption file to the captions folder on
    // S3 as well for data analysis purpose
    if (targetLang === 'en') {
      const captionUploadParams = {
        Bucket: BUCKET_NAME,
        Key: `captions/${mediaId}_${targetLang}.vtt`,
        Body: captionRes.data,
      };

      functions.logger.info('Start uploading .vtt to the captions folder...');
      await S3.upload(captionUploadParams).promise();
    }

    functions.logger.info('Create new media object for the caption...');
    const newMediaRef = db.collection('media').doc();
    await newMediaRef.set({
      src: `s3://${BUCKET_NAME}/${filePath}`,
      type: 'caption',
      srcLang: srcLang,
      targetLang: targetLang,
      youtube: caption.id,
      submission: `submissions/${submissionId}`,
      media: `media/${mediaId}`,
      createdAt: FieldValue.serverTimestamp(),
    });
    await db.doc(`submissions/${submissionId}`).update({ media: admin.firestore.FieldValue.arrayUnion(newMediaRef) });
  } catch (error) {
    functions.logger.error('Get Video Caption Error: ', error);
    return error;
  }
}

/**
 * Insert new YouTube caption to a specific video
 *
 * @param {*} mediaId Media ID
 * @param {*} language Languaage for the submnission
 * @param {*} captionData Caption data in .vtt format
 */
async function insertYouTubeCaption(mediaId, language, captionData) {
  try {
    // Get YouTube video id from the media object
    const mediaSnapshot = await db.collection('media').doc(mediaId).get();
    const youtubeId = mediaSnapshot.data().youtube;

    // Get the Google OAuth 2.0 client
    const oauth2Client = await getOAuthClient();

    // Save the caption to a local file
    const tmpPath = path.join(os.tmpdir(), `${mediaId}.vtt`);
    fs.writeFileSync(tmpPath, captionData);
    const fileStream = fs.createReadStream(tmpPath);

    // Construct the YouTube captions list parameter
    const params = {
      auth: oauth2Client,
      part: 'snippet',
      resource: {
        snippet: {
          language: language,
          videoId: youtubeId,
          name: '',
          isDraft: false,
        },
      },
      media: {
        mimeType: '*/*',
        body: fileStream,
      },
    };

    const result = await YoutubeApi.captions.insert(params);
    functions.logger.info('YoutubeApi Captions Insert Result Status: ', result.status);
    fs.unlinkSync(tmpPath);
  } catch (error) {
    functions.logger.error('Insert Video Caption Error: ', error);
    return error;
  }
}

/**
 * Get refresh token from Firestore and generate access token using the refresh
 * token retrieved, return the OAuth 2.0 client for YouTube Data API requests
 *
 * @returns {OAuth2Client} Google OAuth 2.0 Client
 */
async function getOAuthClient() {
  // Get refresh token from Firestore
  const tokenId = environment.google.token_id;
  const tokenRef = db.collection('tokens').doc(tokenId);
  const snapshot = await tokenRef.get();
  const refreshToken = snapshot.data().oauth.refresh_token;

  // Initialize Google OAuth client
  const oauth2Client = new google.auth.OAuth2(
    environment.google.client_id,
    environment.google.client_secret,
    environment.google.redirect_urls,
  );

  // Get access token with refresh token & reset the token in Firestore
  oauth2Client.credentials.refresh_token = refreshToken;
  const response = await oauth2Client.getAccessToken();
  const credentials = response.res.data;
  oauth2Client.setCredentials(credentials);
  await tokenRef.update({ oauth: credentials });
  return oauth2Client;
}

/**
 * Get user name based on the uid
 *
 * @param {*} uid User ID
 */
async function getUserName(uid) {
  const user = (await db.collection('users').doc(uid).get()).data();

  let userName;
  if (user && user.user_name && user.user_name !== 'Reporter') userName = user.user_name;
  else {
    firebaseUser = await auth.getUser(uid);
    if (firebaseUser.displayName) userName = firebaseUser.displayName;
    else userName = 'Reporter';
  }
  functions.logger.info('userName: ', userName);
  return userName;
}

async function getSubmissionLang(mediaObj) {
  // const socialMedia = (await db.collection('config').doc('socialMedia').get()).data();

  const submission = (await db.doc(mediaObj.submission).get()).data();
  // const phase = submission.phase;
  const language = submission.language;
  // const uid = submission.submitted_by.id;
  // functions.logger.info('uid: ', uid);
  // const userName = await getUserName(uid);

  // let title = socialMedia[`youtube_title_${phase}`][language];
  // if (!title) title = socialMedia[`youtube_title_${phase}`]['en'];

  // const date = DateTime.fromSeconds(submission.createdAt._seconds)
  //   .setLocale(language)
  //   .toLocaleString({ month: 'long', day: 'numeric' });

  // title = title.replace('{0}', userName);
  // title = title.replace('{1}', date);
  // functions.logger.info('title: ', title);
  return language;
}

/**
 * Get the submission titkle by parsing the user name
 *
 * @param {*} mediaObj Media Object
 */
async function getSubmissionTitle(mediaObj) {
  const socialMedia = (await db.collection('config').doc('socialMedia').get()).data();

  const submission = (await db.doc(mediaObj.submission).get()).data();
  const phase = submission.phase;
  const language = submission.language;
  const uid = submission.submitted_by.id;
  functions.logger.info('uid: ', uid);
  const userName = await getUserName(uid);

  let title = socialMedia[`youtube_title_${phase}`][language];
  if (!title) title = socialMedia[`youtube_title_${phase}`]['en'];

  const date = DateTime.fromSeconds(submission.createdAt._seconds)
    .setLocale(language)
    .toLocaleString({ month: 'long', day: 'numeric' });

  title = title.replace('{0}', userName);
  title = title.replace('{1}', date);
  functions.logger.info('title: ', title);
  return title;
}

/**
 * Get hd_video media object
 *
 * @param {*} sid Submission ID
 */
async function getHdMediaObj(sid) {
  const mediaObjects = await db.collection('media').where('submission', '==', `submissions/${sid}`).get();

  let mediaObj;
  for (let i = 0; i < mediaObjects.size; i++) {
    const media = mediaObjects.docs[i];
    if (media.data().type === 'hd_video' && media.data().status !== 'exporting') {
      mediaObj = media;
      break;
    }
  }
  return mediaObj;
}

/**
 * Get the submission description by parsing the team member array and tags
 *
 * @param {*} mediaObj Media Object
 */
async function getSubmissionDescription(mediaObj) {
  const socialMedia = (await db.collection('config').doc('socialMedia').get()).data();
  const submission = (await db.doc(mediaObj.submission).get()).data();
  const phase = submission.phase;
  const language = submission.language;
  const nationSociety = find(ifrcList, { code: submission.region }).ns;
  const uid = submission.submitted_by.id;
  functions.logger.info('uid: ', uid);
  const userName = await getUserName(uid);

  let description = socialMedia[`youtube_description_${phase}`][language];
  if (!description) description = socialMedia[`youtube_description_${phase}`]['en'];
  description = description.replace(/\\n/g, '\n');
  description = description.replace('{0}', nationSociety);

  const { team, tags } = submission;
  let tagStr = '#ClimateRedTV ';
  tags.forEach(tag => {
    tagStr += `${capitalizeTag(tag)} `;
  });
  description = description.replace('{1}', `\n${tagStr}\n\n`);

  let teamStr = `${userName}`;
  if (team && team.length !== 0) {
    team.forEach(member => {
      teamStr += `${member.name} `;
    });
  }
  description = description.replace('{2}', teamStr);

  await db.doc(mediaObj.submission).update({ description: description });
  return description;
}

/**
 * Format the tags
 *
 * @param {*} str String to be formatted
 */
function capitalizeTag(str) {
  const arr = str.toLowerCase().replace('#', '').split('_');

  for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].substring(1);
  }
  return '#' + arr.join('');
}

exports.pushToYouTube = pushToYouTube;
exports.updateYouTubeDescription = updateYouTubeDescription;
exports.getYouTubeCaption = getYouTubeCaption;
exports.insertYouTubeCaption = insertYouTubeCaption;
exports.getOAuthClient = getOAuthClient;
exports.getUserName = getUserName;
