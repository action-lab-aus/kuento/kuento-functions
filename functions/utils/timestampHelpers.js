exports.timeStampToSeconds = timeStamp => {
  const hoursMinutesSeconds = timeStamp.split(':');
  return (
    Number(hoursMinutesSeconds[0]) * 60 * 60 +
    Number(hoursMinutesSeconds[1]) * 60 +
    Number(hoursMinutesSeconds[2].split(',').join('.'))
  );
};

exports.secondsToTimeStamp = seconds => {
  const date = new Date(seconds * 1000).toISOString().substr(11, 11); // Get the timestamp
  return `${date.substr(0, 8)},${date.substr(9)}`; // Replace the decimal point with a comma
};
