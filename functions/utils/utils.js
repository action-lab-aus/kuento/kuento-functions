const functions = require('firebase-functions');
const admin = require('firebase-admin');
const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;
const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const BUCKET_NAME = 'cop27';

/**
 * Get no of videos for each phase
 */
async function getNoOfVideos() {
  const configSnapshot = await db.collection('config').doc('meta').get();

  let phaseVideos = {};
  const phases = configSnapshot.data().phases;
  phases.forEach(p => {
    phaseVideos[p.code] = p.noOfVideos;
  });

  return phaseVideos;
}

/**
 * Upload caption files to Amazon S3
 *
 * @param {*} submissionId Submission ID
 * @param {*} mediaId Media ID
 * @param {*} language Language for the caption
 * @param {*} captionData Caption data in .vtt format
 * @param {*} youtube YouTube video ID
 */
async function uploadCaptionToS3(submissionId, mediaId, language, captionData, youtube) {
  const filePath = `submissions/${submissionId}/${mediaId}/${mediaId}_${language}.vtt`;
  const uploadParams = {
    Bucket: BUCKET_NAME,
    Key: filePath,
    Body: captionData,
  };

  functions.logger.info('Start uploading .vtt to S3...');
  await S3.upload(uploadParams).promise();

  const newMediaRef = db.collection('media').doc();
  await newMediaRef.set({
    src: `s3://${BUCKET_NAME}/${filePath}`,
    type: 'caption',
    srcLang: language,
    targetLang: language,
    youtube: youtube,
    submission: `submissions/${submissionId}`,
    media: `media/${mediaId}`,
    createdAt: FieldValue.serverTimestamp(),
  });
  await db.doc(`submissions/${submissionId}`).update({ media: admin.firestore.FieldValue.arrayUnion(newMediaRef) });
}

exports.getNoOfVideos = getNoOfVideos;
exports.uploadCaptionToS3 = uploadCaptionToS3;
